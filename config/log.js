var winston = require('winston')


function formatter (obj) {
  var z = new Date()
  return z.toLocaleString()+' | '+ obj.level +' | '+obj.message
}

var customLogger = new winston.Logger({
    transports: [
      new winston.transports.File({
          name: 'logger',
          level: 'debug',
          maxsize: 1024000,
          maxFiles: 10,
          formatter: formatter,
          timestamp: false,
          json: false,
          filename: './logs/logger.log',
      })
    ],
    exceptionHandlers: [
      new winston.transports.File({ 
        prettyPrint: true,
        json: false,
        filename: './logs/exceptions.log' 
      })
    ]
});



module.exports.log = {

  colors: false, 
  custom: customLogger,
  level: 'info'

};