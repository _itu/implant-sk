/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  'get /': 'event.list',

  'get /event': 'event.list',
  'get /event/new': 'event.new',
  'get /event/page/:page': 'event.list',
  'get /event/enrolledList': 'event.enrolledList',
  'get /event/:slug': 'event.item',
  'get /event/:slug/edit': 'event.edit',
  'get /event/:slug/remove': 'event.remove',
  'get /event/:id/enrolled': 'event.enrolled',
  'post /event': 'event.create',
  'post /event/:slug': 'event.update',
  'post /event/:id/change': 'event.ajaxUpdate',


  'get /knowledge': 'knowledge.list',
  'get /knowledge/new': 'knowledge.new',
  'get /knowledge/page/:page': 'knowledge.list',
  'get /knowledge/:id': 'knowledge.item',
  'get /knowledge/:id/remove': 'knowledge.remove',
  'get /knowledge/:id/edit': 'knowledge.edit',
  'post /knowledge': 'knowledge.create',
  'post /knowledge/:id': 'knowledge.update',


  'get /post': 'post.list',
  'get /post/page/:page': 'post.list',
  'get /post/new': 'post.new',
  'get /post/:id/remove': 'post.remove',
  'get /post/:id/edit': 'post.edit',
  'get /post/:id': 'post.item',
  'post /post': 'post.create',
  'post /post/:id': 'post.update',


  'post /comment': 'comment.create',
  'post /comment/:id': 'comment.update',


  'get /news': 'news.list',
  'get /news/page/:page': 'news.list',
  'get /news/new': 'post.new',
  'get /news/:id/edit': 'post.edit',
  'get /news/:id/remove': 'post.remove',
  'get /news/:id': 'post.item',
  'post /news': 'post.create',
  'post /news/:id': 'post.update',


  'get /user/:id/edit': 'user.edit',
  'get /user/:id': 'user.item',
  'post /user/:id': 'user.update',
  'post /user': 'user.create',
  'get /user/:id/messages/:partner': 'user.messages',
  'get /user/:id/messages': 'user.dialogs',
  'post /user/:id/favorites/user/add/:friend_id': 'user.addFavoriteUser',
  'post /user/:id/favorites/user/remove/:friend_id': 'user.removeFavoriteUser',
  'post /user/:id/favorites/post/add/:post_id': 'user.addFavoritePost',
  'post /user/:id/favorites/post/remove/:post_id': 'user.removeFavoritePost',
  'post /user/:id/favorites/tag/add/:tag_id': 'user.addFavoriteTag',
  'post /user/:id/favorites/tag/remove/:tag_id': 'user.removeFavoriteTag',
  'post /user/:id/favorites/event/add/:event_id': 'user.addFavoriteEvent',
  'post /user/:id/favorites/event/remove/:event_id': 'user.removeFavoriteEvent',
  'post /user/:id/eventEnroll': 'user.enrollToEvent',
  'get /user/:id/favorites': 'user.favorites',
  'get /user/:id/published': 'user.published',
  'post /user/:id/change': 'user.ajaxUpdate',
  // 'get /user/:id/feed': 'user.feed',


  'get /company': 'company.item',
  'get /company/new': 'company.new',
  'get /company/edit': 'company.edit',
  'get /company/employee': 'company.employee',
  'get /company/published': 'company.published',
  'get /company/events': 'company.events',
  'get /company/:id': 'company.redirect',
  'post /company/:id': 'company.update',
  'post /company': 'company.create',


  'get /tag/new': 'tag.new',
  'get /tag/:id': 'tag.item',
  'get /tag/:id/edit': 'tag.edit',
  'get /tag/:id/remove': 'tag.remove',
  'post /tag': 'tag.create',
  'post /tag/:id': 'tag.update',


  'post /message': 'message.create',
  // 'get /message/:id/remove': 'message.remove',

  'get /session/signin': 'session.signin',
  'get /session/signout': 'session.signout',
  'get /session/confirm': 'session.verify_email',
  'get /session/forgotten': 'session.recovery_request',
  'post /session/recovery': 'session.recovery',
  'get /session/new_password': 'session.new_password',
  'post /session/recovery_password': 'session.recovery_password',


  'get /ajax/tag': 'ajax.tag_list',
  'get /ajax/user': 'ajax.user_list',
  'get /ajax/company': 'ajax.company_list',
  'get /test': 'ajax.test',


  'post /upload': 'file.upload',
  'get /search': 'search.index'

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
