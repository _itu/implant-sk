module.exports.policies = {

  '*': 'auth_logic',

  UserController: {
    '*': 'auth_logic',
    'update': ['auth_logic','is_auth', 'is_own_id'],
    'edit': ['auth_logic','is_auth', 'is_own_id'],
    'messages': ['auth_logic','is_auth', 'is_own_id'],
    'dialogs': ['auth_logic','is_auth', 'is_own_id'],
    'update': ['is_auth', 'is_own_id'],
    'addFavoriteUser': ['is_auth', 'is_own_id'],
    'removeFavoriteUser': ['is_auth', 'is_own_id'],
    'addFavoritePost': ['is_auth', 'is_own_id'],
    'removeFavoritePost': ['is_auth', 'is_own_id'],
    'addFavoriteTag': ['is_auth', 'is_own_id'],
    'removeFavoriteTag': ['is_auth', 'is_own_id'],
    'addFavoriteEvent': ['is_auth', 'is_own_id'],
    'removeFavoriteEvent': ['is_auth', 'is_own_id'],
    'enrollToEvent': ['is_auth', 'is_own_id'],
    'favorites': ['auth_logic','is_auth', 'is_own_id'],
    'published': ['auth_logic','is_auth', 'is_own_id'],
    'ajaxUpdate': ['is_auth', 'is_own_id']
  },

  SessionController: {
    '*': true,
    'signout': 'is_auth'
  },

  PostController: {
    '*': 'auth_logic',
    'new': ['auth_logic','is_auth', 'is_writer'],
    'create': ['is_auth', 'is_writer'],
    'edit': ['auth_logic','is_auth', 'is_post_author'],
    'update': ['is_auth', 'is_post_author'],
    'delete': ['is_auth', 'is_post_author']
  },

  CommentController: {
    '*': true,
    'create': 'is_auth',
    'update': ['is_auth', 'is_comment_author']
  },

  EventController: {
    '*': 'auth_logic',
    'new': ['is_organizer', 'auth_logic'],
    'enrolledList': ['is_organizer', 'auth_logic'],
    'enrolled': ['is_organizer', 'is_event_author', 'auth_logic'],
    'edit': ['is_organizer', 'is_event_author', 'auth_logic'],
    'ajaxUpdate': ['is_organizer', 'is_event_author'],
    'create': 'is_organizer',
    'update': ['is_organizer', 'is_event_author'],
    'delete': ['is_organizer', 'is_event_author']
  },

  KnowledgeController: {
    '*': 'auth_logic',
    'new': ['is_admin', 'auth_logic'],
    'edit': ['is_admin', 'auth_logic'],
    'create': 'is_admin',
    'update': 'is_admin',
    'delete': 'is_admin'
  },

  TagController: {
    '*': 'auth_logic',
    'new': ['is_admin', 'auth_logic'],
    'edit': ['is_admin', 'auth_logic'],
    'create': 'is_admin',
    'update': 'is_admin',
    'delete': 'is_admin'
  },

  CompanyController: {
    '*': 'auth_logic',
    'edit': ['is_admin', 'auth_logic'],
    'new': false
  },

  FileController: {
    'upload': 'is_auth'
  },

  AjaxController: {
    '*': ['is_auth', 'auth_logic']
  }

};
