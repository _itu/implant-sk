var fs = require('fs')
  , path = require('path')

/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
  
  var addFunc = [

      function (next) {

        Company.count().exec(function (err, count) {
          if (err) return next(err)
          if (count > 0) return next()
          
          var company = {
            name: 'ООО Северо-Кавказский медицинский учебно-методический центр',
            legend: '<p>Созданный в 2005г. на базе Клиники реконструктивной стоматологии ИП Долгалева А.А., ООО «Северо-Кавказский медецинский учебно-методический центр»</p><p>В центре работает 10 врачей, 6 аттестованы по различным категориям. Мы применяем современные технологии и уверенно владеем проверенными способами лечения и протезирования. Наш медицинский центр разработал комплексные программы по обучению и подготовке специалистов высокого уровня.</p>',
            admin: 1,
            director_fio: 'Долгалев Александр Александрович',
            abbr: 'СКМУМЦ'
          }

          Company.create(company).exec(next)
        })
      }

  ];

  async.series(addFunc,
    function (err, result) {
      if (err) {
        sails.log.error('cant push fake data');
        return cb(err)
      }

      var uploadsSource = path.join(sails.config.appPath, 'uploads')
        , uploadsDest = path.join(sails.config.appPath, '.tmp/public/uploads')

      if (process.env.NODE_ENV == 'production') fs.symlink(uploadsSource, uploadsDest, cb);
      else cb();
    }
  )

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  // cb();
};
