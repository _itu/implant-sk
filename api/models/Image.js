module.exports = {

  attributes: {

    title: 'string',

    alt: 'string',

    url: {
      type: 'string',
      required: true
    },

    getThumb: function () {
      return this.url.slice(0, -4)+'_thumb.jpg'
    },

    getPreview: function () {
      return this.url.slice(0, -4)+'_preview.jpg'
    },

  }
};

