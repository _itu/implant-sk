/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {
    is_image: Validate.is_image,
    is_date: Validate.is_date
  },

  attributes: {

    locale: {
      type: 'string',
      enum: sails.config.i18n.locales
    },

    avatar: {
      type: 'string',
      is_image: true
    },

    type: {
      type: 'string',
      enum: ['administrator', 'moderator', 'seo', 'member'],
      defaultsTo: 'member'
    },

    first_name: {
      type: 'string',
      required: true
    },

    last_name: {
      type: 'string',
      required: true
    },

    second_name: 'string',

    display_name: 'string',

    title: 'string',

    job_title: 'string',

    email: {
      type: 'string',
      email: true,
      required: true,
      unique: true
    },

    encryptedPassword: {
      type: 'string'
    },

    legend: {
      type: 'text'
    }, 

    sex: {
      type: 'string',
      enum: ['male', 'female']
    },

    birthday: {
      type: 'string',
      is_date: true
    },

    company_id: {
      model: 'company'
    },

    votes_for_answer: {
      collection: 'answer',
      via: 'votes',
      dominant: true
    },

    favorite_events: {
      collection: 'event',
      via: 'subscribers',
      dominant: true
    },

    speaker_of_events: {
      collection: 'event',
      via: 'speakers'
    },
    
    interests: {
      collection: 'tag',
      via: 'with_users',
      dominant: true
    },

    favorite_users: {
      collection: 'user',
      via: 'friend_of',
      dominant: true
    },

    friend_of: {
      collection: 'user',
      via: 'favorite_users'
    },

    educations: {
      collection: 'education',
      via: 'user_id'
    },

    contact_id: {
      model: 'contact'
    },


    is_bd_visible: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    },

    is_writer: {
      type: 'boolean',
      defaultsTo: false
    },

    is_speaker: {
      type: 'boolean',
      defaultsTo: false
    },

    is_organizer: {
      type: 'boolean',
      defaultsTo: false
    },


    is_online: 'boolean',

    is_email_verified: {
      type: 'boolean',
      defaultsTo: false
    },

    verification_token: 'string',

    toJSON: function() {
      var obj = this.toObject()
      delete obj.encryptedPassword
      delete obj.verification_key
      delete obj.is_email_verified
      delete obj.email
      return obj
    }

  },

  beforeValidate: function (values, next) {
    if (values.type) delete values.type
    Tools.clear_empty_values(values);
    next()
  },


  is_friends: function (auth_id, user_id, next) {
    User.findOneById( auth_id )
    .populate('favorite_users', { id: user_id })
    .exec(function (err, user) {
      if (err) {
        sails.log.error(err)
        return next(err)
      }
      if ( _.isEmpty(user.favorite_users) ) next(null, false);
      else next(null, true)
    })
  },


  is_interest: function (values, next) {

    var tags = values.tags
      , tagIdArr = tags.map(function (item) {return item.id})

    User.findOneById( values.auth_id )
    .populate('interests', { id: tagIdArr })
    .exec(function (err, user) {
      if (err) return next(err)

      var temp;
      user.interests.forEach(function (item) {
        if ( (temp = tagIdArr.indexOf(item.id)) != -1) tags[temp].in_interests = true
      })

      next(null, tags)
    })
  },


  beforeCreate: function (values, next) {

    if (values.is_email_verified) {};

    if (!values.password || values.password != values.password_confirm) 
      return next({err: "password not confirmed"})

    var bcrypt = require('bcryptjs')
      , fnArr = [

        function (cb) {
          bcrypt.genSalt(10, cb)
        },

        function (salt, cb) {
          bcrypt.hash(values.password, salt, function (err, encryptedPassword) {
            if (err) return cb(err)
            values.encryptedPassword = encryptedPassword
            cb()
          })
        }

      ]

    async.waterfall(fnArr, function (err) {
      if (err) return next(err)
      
      values.type = 'member'
      values.first_name = Tools.capitalize( values.first_name )
      values.last_name = Tools.capitalize( values.last_name )
      values.second_name && (values.second_name = Tools.capitalize( values.first_name ))
      values.display_name = values.last_name + ' ' + values.first_name.charAt(0).toUpperCase() + '.' + ( values.second_name? values.second_name.charAt(0).toUpperCase()+'.' : '' )
      next();
    })
  }

};

