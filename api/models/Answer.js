/**
* Answer.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    author_id: {
      model: 'user'
    },

    on_question: {
      model: 'question'
    },

    message_id: {
      model: 'message'
    },

    post_id: {
      model: 'post'
    },

    votes: {
      collection: 'user',
      via: 'votes_for_answer'
    }

  }
};

