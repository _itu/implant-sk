/**
* Message.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (html) {
      return Validate.no_script(html)
    },

  },

  attributes: {

    html: {
      type: 'text',
      required: true,
      no_script: true
    },

    sender_id: {
      model: 'user',
      required: true
    },

    recipient_id: {
      model: 'user'
    },

    is_received: {
      type: 'boolean',
      defaultsTo: false
    }

  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  },


  afterCreate: function (values, next) {
    
    Dialog.find({ or: [
      {user_id: values.sender_id, partner_id: values.recipient_id}, 
      {user_id: values.recipient_id, partner_id: values.sender_id}
    ]})
    .exec(function (err, dialogs) {
      if (err) {
        sails.log.error(err)
        next(err)
      }

      async.each(dialogs, function (dialog, cb) {
        dialog.last_message_id = values.id;
        dialog.save(cb)
      }, function (err) {
        if (err) {
          sails.log.error(err)
          next(err)
        }

        next()
      })
    })
  }

};

