/**
* Education.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    user_id: {
      model: 'user',
      required: true
    },

    city: 'string',

    place: {
      type: 'string',
      required: true
    },

    profession: {
      type: 'string',
      required: true
    },

    start_date: 'date',

    end_date: 'date'

  },

  beforeValidate: function (values, next) {
    if (!values.start_date) delete values.start_date;
    if (!values.end_date) delete values.end_date;
    next()
  }

};

