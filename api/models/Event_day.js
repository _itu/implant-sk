/**
* Event_day.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    event_id: {
      model: 'event',
      required: true
    },

    description: 'text',

    day_number: {
      type: 'integer', 
      required: true
    },

    day_start: {
      type: 'datetime', 
      required: true 
    },

    day_end: 'datetime',

    parts: {
      collection: 'event_part',
      via: 'event_day_id'
    }

  },

  beforeValidate: function (values, next) {
    values.day_end = values.day_end? Transform.time_value_to_datetime(values.day_end) : null
    values.day_start = values.day_start? Transform.time_value_to_datetime(values.day_start) : null
    next()
  }
};

