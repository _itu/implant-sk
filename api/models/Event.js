/**
* Event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (html) {
      return Validate.no_script(html)
    },

    is_image: function (title_img) {
      return Validate.is_image(title_img)
    }

  },

  attributes: {

    author_id: {
      model: 'user',
      required: true
    },

    seo_keywords: 'string',
    seo_description: 'string',

    slug: {
      type: 'string',
      unique: true,
      required: true
    },

    lang: {
      type: 'string',
      required: true,
      enum: ['ru', 'en']
    },

    title_img: {
      type: 'string',
      is_image: true
    },

    name: {
      type: 'string',
      required: true
    },

    topics: {
      type: 'array',
      required: true
    },

    description: {
      type: 'text',
      no_script: true
    },

    needs: 'text',

    date: {
      type: 'datetime',
      required: true
    },

    type: {
      type: 'string',
      enum: ['seminar', 'expo', 'webinar', 'conference', 'presentation', 'forum'],
      required: true
    },

    tags: {
      collection: 'tag',
      via: 'with_events',
      dominant: true
    },

    ticket_price: {
      type: 'array',
    },

    speakers: {
      collection: 'user',
      via: 'speaker_of_events',
      dominant: true
    },

    contact_id: {
      model: 'contact'
    },

    subscribers: {
      collection: 'user',
      via: 'favorite_events'
    },

    duration: {
      type: 'integer',
      required: true
    },

    days: {
      collection: 'event_day',
      via: 'event_id'
    },

    is_moderated: {
      type: 'boolean',
      defaultsTo: false
    }

  },
  

  beforeCreate: function (values, next) {
    if (_.isEmpty(values.price)) values.price = ['0'];
    next()
  },


  getEvent: function (slug, next) {
    Event.findOneBySlug(slug)
    .populate('speakers')
    .populate('tags')
    .exec(function (err, event) {
      if (err) return next(err)
      if (!event) return next('Ненайдено мероприятие');
      
      var fnArr = [

        function (cb) {
          if (!event.contact_id) return cb()
          Contact.getContact(event.contact_id, cb)
        },

        function (cb) {
          Event_day.find({event_id: event.id}).sort('day_number ASC')
          .populate('parts', {sort: 'ordinal_number ASC'})
          .exec(function (err, days) {
            if (err) return cb(err)
            cb(null, days)
          })
        },

        function (cb) {
          Participants_of_event.find({event_id: event.id})
          .populate('company_id')
          .exec(function (err, participants) {
            if (err) return cb(err)
            cb(null, participants)
          })
        }

      ]

      async.parallel(fnArr, function (err, results) {
        if (err) return next(err)
        event.contacts = results[0]
        event.event_days = results[1]
        event.participants = results[2]
        next(null, event)
      })
    })
  }
};

