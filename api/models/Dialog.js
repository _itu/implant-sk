/**
* Dialog.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    user_id: {
      model: 'user',
      required: true
    },

    partner_id: {
      model: 'user',
      required: true
    },

    last_message_id: {
      model: 'message'
    }

  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  },

};

