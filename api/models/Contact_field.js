/**
* Contact_field.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    contact_id: {
      model: 'contact',
      required: true
    },

    value: {
      type: 'string',
      required: true
    },

    type: {
      type: 'string',
      enum: ['skype', 'email', 'mobile_phone', 'city_phone', 'global_phone', 'vkontakte', 'facebook', 'twitter', 'linkedid', 'google', 'webpage', 'address', 'latlng'],
      required: true
    }

  }
};

