/**
* Company.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (legend) {
      return Validate.no_script(legend)
    },

    is_image: function (title_image) {
      return Validate.is_image(title_image)
    }

  },

  attributes: {

    questions: {
      collection: 'question',
      via: 'company_id'
    },

    name_ru: {
      type: 'string',
      required: true,
      unique: true
    },

    name_en: {
      type: 'string',
      required: true,
      unique: true
    },

    abbr: {
      type: 'string',
      required: true,
      unique: true
    },

    legend_ru: {
      type: 'text',
      no_script: true
    },

    legend_en: {
      type: 'text',
      no_script: true
    },

    banner: {
      type: 'string',
      is_image: true
    },

    logo: {
      type: 'string',
      is_image: true
    },

    employee: {
      collection: 'user',
      via: 'company_id'
    },

    admin: {
      model: 'user',
      required: true
    },

    director_fio: {
      type: 'string',
      required: true 
    },

    director_id: {
      model: 'user'
    },

    our_tag: {
      model: 'tag'
    },

    contacts: {
      collection: 'contact', 
      via: 'company_id'
    }

  },


  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  },


  beforeCreate: function (values, next) {

    var tag = {
      name: "Блог компании "+values.abbr
    }

    Tag.create(tag).exec(function (err, created) {
      if (err) {
        sails.log.error(err)
        return next(err)
      }
      values.our_tag = created.id
      next()
    })

  }

};

