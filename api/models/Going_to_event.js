
module.exports = {

  attributes: {

    user_id: {
      model: 'user',
      required: true
    },

    event_id: {
      model: 'event',
      required: true
    },

    phone: 'string',

    is_went: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    },

    is_handled: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    },

  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  },

  beforeCreate: function (values, next) {
    Going_to_event.count({
      user_id: values.user_id,
      event_id: values.event_id
    }).exec(function (err, count) {
      if (err) {
        sails.log.error(err)
        return next(err)
      }

      if (count > 0) return next({err: 'Already created'});
      next()
    })
  }

};

