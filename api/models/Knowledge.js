/**
* Knowledge.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (html) {
      return Validate.no_script(html)
    },

    is_image: function (title_image) {
      return Validate.is_image(title_image)
    }

  },

  attributes: {

    seo_keywords: 'string',
    seo_description: 'string',

    name_ru: {
      type: 'string',
      required: true,
      unique: true
    },

    name_en: {
      type: 'string',
      required: true,
      unique: true
    },

    html_ru: {
      type: 'text',
      no_script: true
    },

    html_en: {
      type: 'text',
      no_script: true
    },

    type: {
      type: 'string',
      enum: ['technology', 'tool', 'base'],
      required: true
    },

    tag_id: {
      model: 'tag'
    }
  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values)
    next()
  }
};

