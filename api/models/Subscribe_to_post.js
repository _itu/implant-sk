/**
* Subscribe_to_post.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    user_id: {
      model: 'user',
      required: true
    },

    post_id: {
      model: 'post',
      required: true
    },

    now_count_comments: {
      type: 'integer',
      required: true
    }

  },

  beforeCreate: function (values, next) {
    
    Subscribe_to_post.count({
      user_id: values.user_id,
      post_id: values.post_id
    }).exec(function (err, count) {
      if (err) {
        sails.log.error(err)
        return next(err)
      }

      if (count > 0) return next({err: 'Already created'});
      next()
      
    })

  },


  afterCreate: function (values, next) {
    Post.findOneById( values.post_id ).exec(function (err, post) {
      if (err) {
        sails.log.error(err)
        return next(err)
      }
      
      post.count_subscribers += 1
      post.save(next)
    })
  },


  afterDestroy: function (values, next) {
    if (_.isEmpty(values)) return next()
    var subscribe = _.isArray(values)? values[0] : values

    Post.findOneById( subscribe.post_id ).exec(function (err, post) {
      if (err) {
        sails.log.error(err)
        return next(err)
      }
      
      post.count_subscribers -= 1
      post.save(next)
    })
  },


  update_count_comments: function (values, next) {
    Post.findOneById(values.post_id).exec(function (err, post) {
      if (err) return next(err);
      if (!post) return next(new Error('Post not found'));

      Subscribe_to_post.findOneById(values.id).exec(function (err, subscribe) {
        if (err) return next(err);
        if (!subscribe) return next(new Error('Subscribe not found'));

        subscribe.now_count_comments = post.count_comments
        subscribe.save(next)

      })
    })
  }
};

