/**
* Post.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (html) {
      return Validate.no_script(html)
    },

    is_image: function (title_image) {
      return Validate.is_image(title_image)
    }

  },

  attributes: {

    seo_keywords: 'string',
    seo_description: 'string',

    title_ru: {
      type: 'string',
      required: true
    },

    title_en: {
      type: 'string',
      required: true
    },

    html_ru: {
      type: 'text',
      no_script: true
    },

    html_en: {
      type: 'text',
      no_script: true
    },

    type: {
      type: 'string',
      required: true,
      enum: ['post', 'news', 'idea']
    },

    author_id: {
      model: 'user',
      required: true
    },

    title_image: {
      type: 'string',
      is_image: true
    },

    count_subscribers: {
      type: 'integer',
      defaultsTo: 0
    },

    count_comments: {
      type: 'integer',
      defaultsTo: 0
    },

    tags: {
      collection: 'tag',
      via: 'with_posts',
      dominant: true
    },

    is_moderated: {
      type: 'boolean',
      defaultsTo: false
    }

  },


  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  }

};

