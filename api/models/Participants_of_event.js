/**
* Participants_of_event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    is_image: function (title_img) {
      return Validate.is_image(title_img)
    }

  },

  attributes: {

    name: {
      type: 'string',
      required: true
    },

    logo: {
      type: 'string',
      is_image: true
    },

    event_id: {
      model: 'event',
      required: true
    },

    company_id: {
      model: 'company'
    },

    role: {
      type: 'string',
      enum: ['organizer', 'general_funder', 'information_funder', 'funder', 'participant']
    }

  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  }

};

