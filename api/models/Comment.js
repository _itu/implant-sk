/**
* Comment.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (html) {
      return Validate.no_script(html)
    }

  },

  attributes: {

    html: {
      type: 'text',
      required: true,
      no_script: true
    },

    post_id: {
      model: 'post',
      required: true
    },

    user_id: {
      model: 'user',
      required: true
    },

    parent_id: {
      model: 'comment'
    }

  },

  beforeCreate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  },

  afterCreate: function (values, next) {
    
    function comments_count_inc(err, post) {

      if (err) {
        throw err;
        return init_search()
      }

      post.count_comments++;
      post.save(function (err, s) {

        if (err) {
          throw err;
          return init_search()
        };

        next()

      })
    }

    function init_search () {
      Post.findOne({id: values.post_id}).exec(comments_count_inc)
    }

    init_search();

  },

  afterDestroy: function (values, next) {
    
    function comments_count_inc(err, post) {

      if (err) {
        throw err;
        return init_search()
      }

      post.count_comments--;
      post.save(function (err, s) {
        
        if (err) {
          throw err;
          return init_search()
        };

        next()

      })
    }

    function init_search () {
      Post.findOne({id: values.post_id}).exec(comments_count_inc)
    }

    init_search();

  }


};

