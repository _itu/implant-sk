/**
* Question.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (html) {
      return Validate.no_script(html)
    }

  },

  attributes: {

    answers: {
      collection: 'answer',
      via: 'on_question'
    },

    author_id: {
      model: 'user',
      required: true
    },

    html: {
      type: 'text',
      required: true,
      no_script: true
    },

    company_id:{
      model: 'company'
    },

    tags: {
      collection: 'tag',
      via: 'with_questions',
      dominant: true
    },

    is_moderated: {
      type: 'boolean',
      defaultsTo: false
    }

  },

  afterCreate: function (values, next) {
    
    function comments_count_inc(err, post) {

      if (err) {
        throw err;
        return init_search()
      }

      post.count_comments++;
      post.save(function (err, s) {
        
        if (err) {
          throw err;
          return init_search()
        };

        next()

      })
    }

    function init_search () {
      Post.findOne({id: 1}).exec(comments_count_inc)
    }

    init_search();

  }

};

