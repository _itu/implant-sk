/**
* Event_part.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    no_script: function (html) {
      return Validate.no_script(html)
    }

  },

  attributes: {

    event_day_id: {
      model: 'event_day'
    },

    part_start: 'datetime',

    part_end: 'datetime',

    speaker: 'string',

    ordinal_number: 'integer', 

    speaker_id: {
      model: 'user'
    },

    price: 'string',

    title: {
      type: 'string',
      required: true
    }, 

    html: {
      type: 'text',
      no_script: true,
    },

  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    values.part_end = values.part_end? Transform.time_value_to_datetime(values.part_end) : null
    values.part_start = values.part_start? Transform.time_value_to_datetime(values.part_start) : null
    next()
  }
};

