/**
* Tag.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    name_ru: {
      type: 'string',
      required: true,
      unique: true
    },

    name_en: {
      type: 'string',
      required: true,
      unique: true
    },

    with_questions: {
      collection: 'question',
      via: 'tags'
    },

    with_events: {
      collection: 'event',
      via: 'tags'
    },

    with_posts: {
      collection: 'post',
      via: 'tags'
    },

    with_users: {
      collection: 'user',
      via: 'interests'
    }

  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  },
};

