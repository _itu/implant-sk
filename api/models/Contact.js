/**
* Contact.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    city: 'string',

    company_id: {
      model: 'company'
    },

    fields: {
      collection: 'contact_field',
      via: 'contact_id'
    }

  },

  beforeValidate: function (values, next) {
    Tools.clear_empty_values(values);
    next()
  },

  getCompanyContacts: function (id, next) {
    Contact.find({company_id: id})
    .populate('fields')
    .exec(next)
  },


  getContact: function (id, next) {
    Contact.findOneById(id)
    .populate('fields')
    .exec(next)
  }
};

