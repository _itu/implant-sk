// Реально ли пользователь указал свой ID
module.exports = function(req, res, next) {
  if (req.session.user.id == req.param('id') || req.session.user.type === 'administrator' || req.session.user.type === 'moderator') {
    return next();
  }

  return res.forbidden('Ой как плохо обманывать! Я же тебя вычислю по АЙ ПИ!')
};
