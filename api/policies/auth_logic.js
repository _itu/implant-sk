module.exports = function (req, res, next) {

  if ( !req.session.user ) return next();

  if (req.session.user.locale) req.setLocale(req.session.user.locale)

  Message.count({recipient_id: req.session.user.id, is_received: false}).exec(function (err, count) {
    if (err) {
      sails.log.error(err)
      return next(err)
    }

    if (count) req.session.user.is_receive_message = count
    else req.session.user.is_receive_message = 0
    next()

  })


}