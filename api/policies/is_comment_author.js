module.exports = function(req, res, next) {
  if ( (req.session.user.type === 'administrator' || req.session.user.type === 'moderator') || (req.session.user.id == req.param('user_id') ) ) return next();
  return res.forbidden('Не лазь куда не просят!')
};