module.exports = function(req, res, next) {

  if (req.session.user.type === 'administrator' || req.session.user.type === 'moderator' ) return next();
  var field = req.param('id')? 'id' : 'slug'
    , value = (field === 'id')? req.param('id') : '"'+req.param('slug')+'"'
  
  User.query('SELECT id FROM event WHERE author_id='+req.session.user.id+' AND '+field+'='+value, function (err, event) {
    if (err) {
      sails.log.error(err)
      return next(err)
    }

    if (_.isEmpty(event)) return res.forbidden('Обманывать плохо однако! Я тебя запомнил!')

    next();
  })
};
