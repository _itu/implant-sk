module.exports = function(req, res, next) {

  if (req.session.user.type === 'administrator' || req.session.user.type === 'moderator' ) return next();

  // User is allowed, proceed to the next policy, 
  // or if this is the last policy, the controller
  Post.query('SELECT id FROM post WHERE author_id='+req.session.user.id+' AND id='+req.param('id'), function (err, post) {
    if (err) {
      sails.log.error(err)
      return next(err)
    }

    if (_.isEmpty(post)) return res.forbidden('Обманывать плохо однако! Я тебя запомнил!')

    next();
  })
};
