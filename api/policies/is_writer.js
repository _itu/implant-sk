module.exports = function(req, res, next) {
  if (req.session.user.is_writer || req.session.user.type === 'administrator' || req.session.user.type === 'moderator') return next()

  return res.forbidden('Обманывать плохо однако! Я тебя запомнил!')
};
