module.exports = function(req, res, next) {
  if (req.session.user && (req.session.user.type === 'administrator' || req.session.user.type === 'moderator') ) return next()
  res.forbidden('Не лазь куда не просят!')
};
