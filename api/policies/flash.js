module.exports = function (req, res, next) {

  if ( _.isEmpty(req.flash()) ) {
    console.log(req.flash());
    return next();
  }

  res.locals.flash = {};
  res.locals.flash = _.clone(req.session.flash);

  // clear flash
  req.session.flash = {};
  next();

}