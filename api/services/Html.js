module.exports = {

  // Создание комментариев
  create_comments: function (comments, padding, locale) {

    var lvl = 0, html = '',
        padding = padding || 30,
        maxPadding = 400;

    var comments = _.groupBy(comments, function (item) {
      return item.parent_id>>0
    })

    function makeComments (num,lvl) {
      var tempPadding = (tempPadding = lvl*padding) < maxPadding? tempPadding : maxPadding;
      comments[num].forEach(function (item) {
        html += '<div class="item" data-id="'+item.id+'" data-author-id="'+item.user_id.id+'" style="padding-left: '+ tempPadding +'px">'+
                '<div class="block_top"><a href="/user/'+item.user_id.id+'" class="authorInfo">'+item.user_id.display_name+'</a><date class="date">'+Transform.date_to_string( item.createdAt, locale ) +' '+ Transform.time_to_string( item.createdAt )+'</date></div>'+
                '<div class="html">'+item.html+'</div></div>'

        if (comments[ item.id ]) makeComments( item.id, lvl+1 );
      })
    }

    makeComments(0, 0)
    return html;
  },


  // список полей в PROFILE
  create_profile: function (source_data, source_schema, locale) {

    var html = ''
      , padding = 20
      , locale = locale || 'en'

    schemaset_handler([] ,source_schema)

    function validate_itemset (source_path, schema) {
      var temp_path, state = false;
      switch(schema.type){

        case 'array':
          if ( get_data(source_path.concat(schema.name)).length ) state = true
          break;


        case 'json':
          temp_path = source_path.concat(schema.name)
          state = validate()
          break;


        case 'group':
          temp_path = source_path
          state = validate()
          break;
      }

      function validate () {
        var temp_data = get_data(temp_path)

        return schema.items.some(function (item) {
          if (item.items) return validate_itemset(temp_path, item);
          else if (item.validate) return [item.name+(schema.localized && !schema.nested? '_'+locale : ''), item.validate].every(function (rule) {return temp_data[rule]})
          else return temp_data[item.name+(schema.localized && !schema.nested? '_'+locale : '')]
        })
      } 

      return state
    }


    function get_data (arr) {
      var result = source_data
        , len = arr.length

      if (_.isEmpty(arr)) return result;

      for (var i = 0; i < len; i++) {
        if (!result) return ;
        result = result[ arr[i] ]
      };

      return result
    }


    function schemaset_handler (source_path, schemaset) {

      var temp_data = get_data(source_path)
      for (var i = 0; i < schemaset.length; i++) {
        var schema = schemaset[i]

        if (schema.items) add_itemset(source_path, schema)
        else {
          var ready_to_push = temp_data? temp_data[ schema.name+(schema.localized && !schema.nested? '_'+locale : '') ] : ''
          if (schema.titleField) schema.title = sails.__({phrase: temp_data[ schema.titleField ], locale: locale})
          if (schema.validateField) schema.validate = !temp_data[ schema.validateField ]
          add_item(ready_to_push, schema)
        }
      }
    }


    function add_itemset (source_path, schema) {
      if (!validate_itemset(source_path, schema)) return
        
      html += '<div style="position: relative; padding-left: '+(padding*source_path.length)+'px">'
      if (schema.title) html += '<h3>'+schema.title+'</h3>'

      switch(schema.type){

        case 'group':
          schemaset_handler( source_path, schema.items )
          break;


        case 'json':
          schemaset_handler( source_path.concat(schema.name), schema.items )
          break;


        case 'array':
          var temp_data = get_data( source_path.concat(schema.name) )
          for (var i = 0; i < temp_data.length; i++) {
            if (schema.items.length > 1) html += '<div class="itemSet">'
            schemaset_handler( source_path.concat([schema.name, i]), schema.items )
            if (schema.items.length > 1) html += '</div>'
          };
          
          break;

      }

      html += '</div>'
    }


    function add_item (data, schema) {
      if (!data || schema.validate ) return;

      var data = !schema.nested
          ? data
          : schema.localized
            ? data[ schema.nested+'_'+locale ] 
            : data[ schema.nested ]
        , title = schema.title

      if (title) html += '<div class="list2Col"'+(schema.params? ' '+schema.params+' ' : '')+'><span class="col1">'+title+':</span><span class="col2">'
      else html += '<p'+(schema.params? ' '+schema.params+' ' : '')+'>'

      switch(schema.type){

        case 'date':
          html += Transform.date_to_string(data, locale, true) 
          break;


        default:
          if (schema.localed) data = sails.__({phrase: data, locale: locale});
          html += data
          break;

      }

      if (title) html += '</span></div>'
      else html += '</p>'
      
    }
    
    return html
  }, 


  // Пагинация мать её
  paginate: function (obj, entity) {

    var now = obj.page_num>>0
      , count = obj.items_count>>0
      , per_page = obj.items_per_page>>0
      , max_pages = Math.ceil(count/per_page)
      , max_visible_pages = 5
      , visible_pages = (max_pages < max_visible_pages)? max_pages : max_visible_pages
      , start_page = now - Math.floor(visible_pages/2)
      , end_page = (end_page = start_page+visible_pages-1)>max_pages? max_pages : end_page
      , html = ''

    if (max_pages <= 1) return;

    if (start_page < 1) {
      end_page = visible_pages
      start_page = 1
    } else if ((max_pages - now) < visible_pages) {
      start_page = end_page - visible_pages + 1
    }

    html+= '<a href="/'+entity+'/page/1" class="pageFirst"></a>'
    if (start_page > 1) html+= '<span>...</span>'

    for (var i = start_page; i <= end_page; i++) {
      html += '<a href="/'+entity+'/page/'+i+'" class="button'+ (i==now? ' active' : '')  +'">'+i+'</a>'
    }

    if (end_page !== max_pages) html+= '<span>...</span>'
    html+= '<a href="/'+entity+'/page/'+max_pages+'" class="pageLast"></a>'

    return html;
  },
 

  'title': function (string) {
    
  },


  edit_entity_form: function (source_data, source_schema, locale) {
    var html = ''
      , padding = 30
      , locale = locale || 'en'

    schemaset_handler([] ,source_schema, false)


    function get_data (arr) {
      if (_.isEmpty(source_data)) return undefined;
      var result = source_data
        , len = arr.length

      if (_.isEmpty(arr)) return result;

      for (var i = 0; i < len; i++) {
        if (!result) return ;
        result = result[ arr[i] ]
      };

      return result
    }


    function get_name (arr, last_name, is_pattern) {
      var result = is_pattern? 'data-name="' : 'name="'
      if (_.isEmpty(arr)) return result+last_name+'" '

      for (var i = 0; i < arr.length; i++) {
        if (i === 0) result += arr[i]
        else result += '['+arr[i]+']'
      };

      return result+'['+last_name+']" '
    }


    function schemaset_handler (source_path, schemaset, is_pattern) {
      var temp_data = get_data(source_path)
      for (var i = 0; i < schemaset.length; i++) {
        var schema = schemaset[i]
        if (schema.items) add_itemset(source_path, schema, is_pattern)
        else {
          var ready_to_push = (schema.before)
            ? get_data( source_path.slice(0, schema.before.level).concat(schema.before.name) )
            : temp_data? temp_data[ schema.name ] : ''
          add_item(ready_to_push, get_name(source_path, schema.name, is_pattern), schema, is_pattern)
        }
      }
    }


    function add_itemset (source_path, schema, is_pattern) {

      html += '<div style="position: relative; padding-left: '+(padding*source_path.length)+'px"><h3>'+schema.title+'</h3>'
      if (schema.description) html+= '<div class="field_description">'+schema.description+'</div>'

      var temp_data = get_data( source_path.concat(schema.name) )
      
      if ( schema.type === 'array' ) {
        
        switch(schema.array_type){

          case 'join':

            html += '<div class="a-createAdditionalField" data-field-type="pseudo" title="Добавить экземпляр">+<div class="itemSet a-pattern" data-setname="'+schema.name+'">'
            var connector = 'for_'+schema.name+'_00'
            html += '<input type="hidden"'+get_name(source_path.concat(schema.name), 0, true)+' id="'+connector+'">'
            for (var i = 0; i < schema.items.length; i++) {
              add_item('', ' class="a-pseudoField" data-connector="'+connector+'" ', schema.items[i], true)
            };
            html += '</div></div>'

            if (_.isEmpty(temp_data) && schema.required) {

              html += '<div class="itemSet" data-setname="'+schema.name+'">'
              connector = 'for_'+schema.name+'_0'
              html += '<input type="hidden"'+get_name(source_path.concat(schema.name), 0, is_pattern)+' id="'+connector+'">'

              for (var i = 0; i < schema.items.length; i++) {
                add_item('', ' class="a-pseudoField" data-connector="'+connector+'" ', schema.items[i], is_pattern)
              };
              html += '</div>'

            } else if (!_.isEmpty(temp_data))  {

              var temp_data_join = temp_data.map(function (item) {return item.split('=')})
              for (var i = 0; i < temp_data_join.length; i++) {
                
                if (i === 0 && schema.required) html += '<div class="itemSet" data-setname="'+schema.name+'">'
                else html += '<div class="itemSet a-canBeDeleted" data-setname="'+schema.name+'">'

                connector = 'for_'+schema.name+'_'+i
                html += '<input type="hidden"'+get_name(source_path.concat(schema.name), i)+' id="'+connector+'" value="'+temp_data[i]+'"">'

                for (var j = 0; j < schema.items.length; j++) {
                  add_item(temp_data_join[i][j], ' class="a-pseudoField" data-connector="'+connector+'" ', schema.items[j], is_pattern)
                };
                html += '</div>'
              }

            }
            break;


          case 'json':

            html += '<div class="a-createAdditionalField" data-field-type="itemset" title="Добавить экземпляр">+<div class="itemSet a-pattern" data-setname="'+schema.name+'">'
            schemaset_handler( source_path.concat([schema.name, 0]), schema.items, true)
            html += '</div></div>'

            if (_.isEmpty(temp_data) && schema.required) {

              html += '<div class="itemSet" data-setname="'+schema.name+'">'
              schemaset_handler( source_path.concat([schema.name, 0]), schema.items, is_pattern )
              html += '</div>'

            } else if (!_.isEmpty(temp_data)) {

              for (var i = 0; i < temp_data.length; i++) {

                if (i === 0 && schema.required) html += '<div class="itemSet " data-setname="'+schema.name+'">'
                else html += '<div class="itemSet a-canBeDeleted" data-setname="'+schema.name+'">'

                schemaset_handler( source_path.concat([schema.name, i]), schema.items, is_pattern )
                html += '</div>'
              };

            }
            break;

        }

      } else if ( schema.type === 'json' ) {
        schemaset_handler( source_path.concat(schema.name), schema.items, is_pattern )
      }

      html += '</div>'
    }


    function add_item (data, name, schema, is_pattern) {
      var data = is_pattern? '' : schema.nested && (schema.type !== 'select') && data? data[ schema.nested ] : data || ''
        , value = !data? ' ' : ' value="'+data+'" '
        , required = schema.required? ' required ' : ' '

      if (schema.type === 'hidden') {
        if (!data && !schema.value) return;
        if (schema.value) value = ' value="'+schema.value+'" '
        html += '<input type="hidden"'+name+value+'>'
        ;return
      };

      html+= '<div class="list2Col">'
      if (schema.required) html+= '<div class="required_label" title="'+ sails.__({phrase: 'required_title', locale: locale}) +'">*</div>'
      if (schema.description) html+= '<div class="field_description">'+schema.description+'</div>'
      html+= '<span class="col1">'+schema.title+':</span><div class="col2">'

      switch(schema.type){

        case 'string':
          html += '<input type="text"'+name+value+required+'>'
          break;


        case 'email':
          html += '<input type="email"'+name+value+required+'>'
          break;


        case 'file':
          html += '<input type="hidden"'+name+value+required+'>'
          html += '<div class="a-popup-upload button press UC" data-target="'+(name.match(/(?:"|')([^"']+)/)[1])+'" data-upload-type="'+schema.file_type+'">'+ sails.__({phrase: 'upload', locale: locale}) +'</div>'
          html += '<img src="'+data+'" class="uploaded_image" />'
          break;


        case 'text':
          if (schema.html) {
            if (!is_pattern) name += ' class="a-ckeditor"'
             name += ' data-ck-type="'+schema.html+'" '
          }
          html += '<textarea '+name+required+'>'+data+'</textarea>' 
          break;


        case 'boolean':
          html += '<input style="display: inline-block" type="radio" value="true"'+name+(data? ' checked' : '')+'><span style="margin-right: 20px; font-size: 15px">'+ sails.__({phrase: 'yes', locale: locale}) +'</span>'
          html += '<input style="display: inline-block" type="radio" value="false"'+name+(!data? ' checked' : '')+'><span style="font-size: 15px">'+ sails.__({phrase: 'no', locale: locale}) +'</span>'
          // html += '<input style="display: block" type="checkbox" value="true"'+name+required+(data? ' checked' : '')+'>'
          break;


        case 'integer':
          html += '<input type="number"'+name+value+required+' title="Введите число">'
          break;


        case 'date':
          html += '<input type="text" class="a-datepicker"'+(data? 'value="'+Transform.datetime_to_date_value(data)+'"' : '')+name+required+'>'
          break;


        case 'time':
          html += '<input type="text"'+(data? 'value="'+Transform.datetime_to_time_value(data)+'"' : '')+name+required+' pattern="(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9])" title="Время в формате NN:NN">'
          break;
          

        case 'select':
          var temp = (name.search('name') != -1) // проверка для pseuso itemset'а
              ? (is_pattern? '' : ' class="a-selectize" ')+name 
              : is_pattern
                ? name 
                : name.slice(0, name.search('class="')+7)+'a-selectize '+ name.slice(name.search('class="')+7)
            , opts = (schema.enum)
              ? " data-options='"+ JSON.stringify( schema.enum.map(function (item) {return {id: item, name: sails.__({phrase: item, locale: locale}) }}) ) +"' " 
              : ' '
            , selected = (data)
              ? " data-selected='"+(_.isArray(data)? data.map(function (item) {return item[schema.nested]}).join(',') : (schema.nested)? data[schema.nested] : data )+"' "
              : (schema.selected)? " data-selected='"+schema.selected+"' " : ' '

        html += '<select '+selected+temp+opts+required+' data-selectize-type="'+schema.select_type+'"'+(schema.ajax? 'data-ajax="'+schema.ajax+'"' : '') +'></select>'
          break;
          
          
        case 'array':
          html += '<div class="fieldSet">';

          html += '<div class="a-createAdditionalField" data-field-type="simple" title="Добавить поле">+<input class="a-pattern" type="text" data-'+name+'></div>';
          if (_.isArray(data) && data.length) {
            for (var i = 0; i < data.length; i++) {
              html += '<input type="text" value="'+data[i]+'" '+name+(i===0? required : 'class="a-canBeDeleted"')+'>';
            };
          } else if (schema.required) html += '<input type="text"'+name+required+'>';

          html += '</div>'
          break;

      }

      html+= '</div></div>'

    }

    return html
  }

}