var util = require('util')

module.exports = {


  capitalize: function (name) {
    return name? name.charAt(0).toUpperCase() + name.slice(1).toLowerCase() : null;
  },


  strip_tags: function (input) {
    if (!input) return '';
    var allowed = (((allowed || '') + '')
      .toLowerCase()
      .match(/<[a-z][a-z0-9]*>/g) || [])
      .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)

    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
    return input.replace(tags, function($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });

  },


  clear_empty_values: function (obj) {
    for (var prop in obj) {
      if (prop && (prop.indexOf('_id') > 0 
        || prop.indexOf('duration') > 0 
        || prop.indexOf('day_number') > 0 
        || prop.indexOf('count_') > 0) 
        && !obj[prop]) {
          delete obj[prop]
      }
    }
  },


  userFavs: function (type, user_id, field, target_id, res, next) {
    User.findOneById( user_id )
    .populate(field)
    .exec(function (err, user) {
      if (err) {
        sails.log.error(err)
        return next(err)
      }
      
      user[field][type]( target_id )
      user.save(function (err) {
        if (err) {
          sails.log.error(err)
          return next(err)
        }
        
        res.ok()
      })
    })
  },


  stringArr_to_numArr: function (arr) {
    return arr.map(function (item) {return item>>0})
  }
  
};