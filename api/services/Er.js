module.exports = {
  
  default: function (req, res, err, type, JSONtype) {
    if (err) {
      sails.log(req.method+': '+req.url)
      sails.log(err)

      if (err.ValidationError) {

        if (req.wantsJSON) 
          res.json( Er.validationErrorHandler(err.ValidationError, req) )
        else {
          Er.validationErrorHandler(err.ValidationError, req)
          res.redirect('back')
        }

      } else {

        if (req.wantsJSON) 
          res.json({errorType: JSONtype || 13})
        else 
          res[ Er.types[type] ]()

      }

      return true
    }else 
      return false
  },

  validationErrorHandler: function (err, req) {
    var msg = Er.types.validate
      , e_res = {errorType: 2, invalid: {} }
      , inv = e_res.invalid

    function withJSON (field, er) {
      ( inv[field] || (inv[field] = []) ).push( makeMsg(field, er.rule) )
    }

    function withFlash (field, er) {
      req.flash('warn', makeMsg(field, er.rule))
    }

    function makeMsg (field, rule) {
      return msg[rule].length === 1
        ? msg[rule][0]
        : msg[rule][0]+' `'+field+'` '+msg[rule][1]
    }


    var fn = req.wantsJSON? withJSON : withFlash
    Object.keys(err).forEach(function (field) {
      err[field].forEach(function (item) {
        if ( !msg[item.rule] ) 
          return sails.log.error('@@@ ДОБАВИТЬ в список ошибок обработку типа ошибки = ', item.rule)
        fn(field, item)
      })
    })

    if (req.wantsJSON) return e_res
  },


  types: {
    500: 'serverError',
    404: 'notFound',
    403: 'forbidden',
    400: 'badRequest',
    validate: {
      unique: ['Такой', 'уже существует'],
      required: ['Нужно обязательно заполнить'],
      before: ['Введенная дата должна быть раньше текущей даты'],
      after: ['Введенная дата должна быть позже текущей даты'],
      is_time: ['Неправильный формат времени'],
      email: ['Направильный формат email`а'],
      minLength: ['Слишком мало символов'],
      maxLength: ['Слишком много символов'],

    }
  }


}

