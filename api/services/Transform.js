module.exports = {

  num_to_word_month: function (num, locale) {
    return Data.month_list[locale][num]
  },


  date_to_string: function (date, locale, bool) {
    date = date.getDate? date : new Date(date);
    return date.getDate() +' '+ Transform.num_to_word_month( date.getMonth(), locale ) + (bool? ' '+ date.getFullYear() : '')
  },

  date_to_range: function (date, duration, locale) {
    if (duration == 1) return Transform.date_to_string(date, locale, true);
    return Transform.date_to_string(date, locale) +' - '+ Transform.date_to_string( new Date( date.getTime() + (86400000*( (duration-1) >> 0))), locale, true)
  },

  time_to_string: function (date) {
    var time = new Date(date)
      , hour = time.getHours()+''
      , min = time.getMinutes()+''
    
    return (hour.length===2? hour : '0'+hour) +':'+ (min.length===2? min : '0'+min)
  },


  datetime_to_date_value: function (datetime) {
    datetime = datetime.getMonth? datetime : new Date(datetime);
    var month = datetime.getMonth()+1+'' 
      , day = datetime.getDate()+''
    return ''+ datetime.getFullYear()+'-'+(month.length===2? month: '0'+month)+'-'+(day.length===2? day: '0'+day)
  },

  datetime_to_time_value: function (datetime) {
    var hours = datetime.getHours()+'' 
      , minutes = datetime.getMinutes()+'' 
    return (hours.length===2? hours: '0'+hours)+':'+(minutes.length===2? minutes: '0'+minutes)
  },

  time_value_to_datetime: function (string) {
    return new Date('0-01-01 '+string)
  },

  validationError_to_flash: function (model, validationError, req) {
    var messages = model.validation_messages || {}
      , errorFields = Object.keys(validationError)
      , trigger = true

    errorFields.forEach(function (field) {

      trigger = true
      if (!messages[field]) trigger = false
      validationError[field].forEach(function (item) {
        
        if (trigger && messages[field][item.rule]) {
          req.flash('warn', messages[field][item.rule])
          return;
        }

        req.flash('warn', item.rule+', '+item.message)
      })
    })
  },

  edit_end_of_word: function (num, str, locale) {
      
    var number = (typeof num == 'number')? String(num) : num
      , result = sails.__({phrase: str+'_5', locale: locale});
   
    if (number.slice(-2, -1) != "1") {
      if (number.slice(-1) == "2" ||
          number.slice(-1) == "3" ||
          number.slice(-1) == "4") result = sails.__({phrase: str+'_2', locale: locale});
      else if (number.slice(-1) == "1") result = sails.__({phrase: str+'_1', locale: locale});
    }
  
    return result;
  }

}