module.exports = {

  no_script: function (text) {
    return text.search(/<\s*script/i) === -1? true : false
  },

  is_image: function (text) {
    return text.search(/((?:[^\/]+.)+\.(?:jpg|png|jpeg))$/) === -1? false : true
  },

  is_date: function (text) {
    return text.search(/^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/) === -1? false : true
  }

}