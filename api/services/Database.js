module.exports = {

  new_update: function (source_data, source_schema, checker, next) {
    var util = require('util')

    if ( _.isEmpty(source_data) || (checker.length && (source_data[ checker[0] ] != checker[1])) ) return


    function get_data (arr) {
      var result = source_data
        , len = arr.length

      if (_.isEmpty(arr)) return result;

      for (var i = 0; i < len; i++) {
        if (!result) return result;
        result = result[ arr[i] ]
      };
      return result
    }

    function get_schema (arr) {
      var result = source_schema
        , len = arr.length

      if (_.isEmpty(arr)) return result;

      for (var i = 0; i < len; i++) {
        if (!result) return result;
        result = result[ arr[i] ]
      };
      return result
    }

    function delete_fields (obj, arr) {
      if (!arr.length) return obj
      var temp_obj = _.clone(obj, true)
      arr.forEach(function (item) {delete temp_obj[item]})
      return temp_obj
    }

    iterate_items([],[], function (err) {
      if (err) return next(err)
        if (source_schema.created[0]) next(null, source_schema.created[0])
        else if (!_.isArray(source_data)) next(null, source_data.slug || source_data.id)
        else next()
    })

    function iterate_items (source_path, schema_path, cb) {
      var temp_data = get_data(source_path)
        , is_array = _.isArray(temp_data)
        , temp_schema = get_schema(schema_path)
        // , back_steps = (is_array)? -2 : -1
        , parent_schema = get_schema(schema_path.slice(0, -2))
        , parent_data = get_data(source_path.slice(0,-1))
        , linked_fields = temp_schema.links
          ? _.chain(temp_schema.links).pluck('by').compact().value()
          : []
        , filter = (temp_schema.not_removed || !source_path.length)
          ? ''
          : temp_schema.via? ' WHERE '+temp_schema.via+'='+parent_data.id
            : (temp_schema.linked_by && parent_data[temp_schema.linked_by])? ' WHERE id='+parent_data[temp_schema.linked_by] : '';
      
      temp_schema.deleted = []
      temp_schema.created = []
      if (!is_array) temp_data = (_.isEmpty(temp_data))? [] : [temp_data]
      var update = _.filter(temp_data, function (item) {return (typeof item === 'object' && item.id)? true : false})
        , create = _.filter(temp_data, function (item) {return (typeof item === 'object' && !item.id)? true : false})

      if (filter && !_.isEmpty(parent_schema.deleted) && temp_schema.via) parent_schema.deleted.forEach(function (item) {filter += ' OR '+temp_schema.via+'='+item})

      function next_level (item, callback) {
        if (temp_schema.links) {
          async.each(temp_schema.links, function (link, callback2) {
            if (is_array) {iterate_items(source_path.concat([temp_data.indexOf(item), link.by]), schema_path.concat(['links', temp_schema.links.indexOf(link)]), callback2)}
            else iterate_items(source_path.concat(link.by), schema_path.concat(['links', temp_schema.links.indexOf(link)]), callback2)
          }, callback)

        } else callback()
      }


      var fnArr = [
    
        function (cb2) {
          if (_.isEmpty(update)) return cb2();
          // console.log('updated start', update)

          async.each(update, function (item, cb3) {
            sails.models[temp_schema.model].update({id: item.id}, delete_fields(item, linked_fields)).exec(function (err, updated) {
              if (err) return cb3(err)
              next_level(item, cb3)
            })
          }, cb2)
          
        },


        function (cb2) {
          if (_.isEmpty(create)) return cb2();

          async.each(create, function (item, cb3) {
            delete item.id
            if (temp_schema.via) item[ temp_schema.via ] = parent_data.id

            // console.log('created start ', create)
            sails.models[temp_schema.model].create( delete_fields(item, linked_fields) ).exec(function (err, created) {
              if (err) return cb3(err)
              item.id = created.id+''
              temp_schema.created.splice(temp_schema.created.length, 0, item.id>>0)
              // console.log('created alrdy', item)

              if (temp_schema.linked_by) {
                User.query('UPDATE '+parent_schema.model+' SET '+temp_schema.linked_by+'='+created.id+' WHERE id='+parent_data.id, function (err) {
                  if (err) return cb3(err)
                  next_level(item, cb3)
                })
              } else next_level(item, cb3)

            })
          }, cb2)
        },
        
        function (cb2) {
          // console.log('deleted start ', filter)

          if (_.isEmpty(filter)) return cb2();
          User.query('SELECT id FROM '+temp_schema.model+filter, function (err, result) {
            // console.log('delete coming ', result, update)
            temp_schema.deleted = _.difference(result.map(function (item) {return item.id>>0}), update.map(function (item) {return item.id>>0}).concat(temp_schema.created))
            if (_.isEmpty(temp_schema.deleted)) return cb2()
            sails.models[temp_schema.model].destroy({id: temp_schema.deleted}).exec(cb2)
          })
        },

      ]
        
      async.series(fnArr, cb)
    }


  }

}