module.exports = {

  month_list: {
    'ru': ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
    'en': ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
  },

  event_icons: {

    white: {
      seminar: '/images/icons/event_seminar_white.png',
      expo: '/images/icons/event_expo_white.png',
      webinar: '/images/icons/event_webinar_white.png',
      conference: '/images/icons/event_conference_white.png',
      presentation: '/images/icons/event_presentation_white.png',
      forum: '/images/icons/event_forum_white.png'
    },

    blue: {
      seminar: '/images/icons/event_seminar.png',
      expo: '/images/icons/event_expo.png',
      webinar: '/images/icons/event_webinar.png',
      conference: '/images/icons/event_conference.png',
      presentation: '/images/icons/event_presentation.png',
      forum: '/images/icons/event_forum.png'
    }
    
  }
  
};