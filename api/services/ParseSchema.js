module.exports = {

  event_price: function (source, locale) {
    var locale = locale || 'ru'
    return (typeof source === 'string'? JSON.parse(source) : source)
      .map(function(item){
        var temp = item.split('=')
        return { 
          text: (!temp[0].length)
            ? sails.__({phrase: 'do_refine', locale: locale}) 
            : (temp[0] === '0')
              ? sails.__({phrase: 'for_free', locale: locale}) 
              : temp[0]+' '+ sails.__({phrase: temp[1], locale: locale}),
          description: temp[2] || null
        }
      })
  },
};