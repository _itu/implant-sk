/**
 * SearchController
 *
 * @description :: Server-side logic for managing searches
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  index: function (req, res, next) {

    var model, result
      , locale = req.getLocale()
      , localed = {
          name: 'name_'+locale,
          title: 'title_'+locale,
          html: 'html_'+locale
        }
      , opts = {}
      , populateFields = []
      , entity = req.param('entity')
      , text = req.param('text')

    switch( entity ){
      case 'tag':
        model = Tag
        populateFields = ['with_posts']
        if (text) {
          if (locale === 'ru') opts = { name_ru: {'contains': text} }
          else opts = { name_en: {'contains': text} }
        }
        break;

      case 'post':
        model = Post
        populateFields = ['author_id']
        if (text) {
          if (locale === 'ru') {
            opts.or = [
              {'title_ru': {'contains': text} },
              {'html_ru': {'contains': text} }
            ]
          } else {
            opts.or = [
              {'title_en': {'contains': text} },
              {'html_en': {'contains': text} }
            ]
          }
          
        }
        break;

      case 'user':
        model = User
        populateFields = ['interests', 'favorite_users']
        opts.is_email_verified = true;
        if (text) opts.or = [
          { first_name: {'contains': text} },
          { last_name: {'contains': text} },
          { second_name: {'contains': text} }
        ]
        break;

      default:
        model = Post
        entity = 'post'
    }

    // if ( req.param('entity') ) opts. req.param('entity');

    result = model.find(opts)

    for (var i = 0; i < populateFields.length; i++) {
      result.populate(populateFields[i])
    };

    result.exec(function (err, results) {
      if ( Er.default(req, res, err, 500) ) return
      if (entity === 'tag' && req.session.user && results[0]) {
        User.is_interest({
          auth_id: req.session.user.id,
          tags: results
        }, function (err, tags) {
          if ( Er.default(req, res, err, 500) ) return
          res.view('search/index', {results: tags, entity: entity})
        })

      } 
      else res.view('search/index', {results: results, entity: entity})

    })
    

  }

};

