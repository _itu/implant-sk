/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'signin': function (req, res) {

    if ( !req.param('email') || !req.param('password') ) {
      req.flash('error', 'u must enter email and password')
      return res.redirect('back')
    }

    User.findOneByEmail( req.param('email'), function (err, user) {
      
      if (err) {
        req.flash('error', 'Ошбибка сервера')
        return res.redirect('back')
      }

      if (!user) {
        req.flash('error', "Пользователя с таким email'ом не зарегистрировано")
        return res.redirect('back')
      }

      if (!user.is_email_verified) {
        req.flash('warn', "Ваш email ещё не подтвержден. Пожалуйста проверьте свою почту.")
        return res.redirect('back')
      };

      require('bcryptjs').compare(req.param('password'), user.encryptedPassword, function (err, result) {
        if (err) {
          req.flash('error', 'Неправильная комбинация логина и пароля')
          return res.redirect('back')
        }

        res.cookie('lang', user.locale || req.getLocale())
        req.session.user = user.toJSON();
        res.redirect('back')
      })
    })

  },


  'signout': function (req, res, next) {
    res.clearCookie('lang')
    req.session.destroy();
    res.redirect('back')
  },

  'verify_email': function (req, res, next) {
    User.findOneByEmail(req.param('email'))
    .exec(function (err, user) {
      if ( Er.default(req, res, err, 500) ) return
      if (!user) {
        req.flash('error', 'Пользователя с такой почтой не зарегистрировано')
        return res.redirect('/')
      }

      if (user.is_email_verified) {
        req.flash('info', 'Ваша учетная запись уже активирована')
        return res.redirect('/')
      }

      if (user.verification_token != req.param('token')) {
        req.flash('error', 'Обманывать нехорошо!')
        return res.redirect('back')
      }

      user.is_email_verified = true
      user.verification_token = null
      user.save(function (err) {
        if ( Er.default(req, res, err, 500) ) return
        req.flash('info', 'Регистрация завершена успешно! Добро пожаловать :)')

        req.session.is_auth = true;
        req.session.user = user;
        res.redirect('/user/' + user.id)
      })
    })
  },


  'recovery_request': function (req, res, next) {
    res.view('session/requestToRecovery')
  },


  'recovery': function (req, res, next) {
    var email = req.param('email')
    User.findOneByEmail(email).exec(function (err, user) {
      if ( Er.default(req, res, err, 500) ) return
      if (!user) {
        req.flash('error', 'Пользователя с таким электронным адресом не зарегистрировано')
        return res.redirect('back')
      }

      var fnArr = [

        function (cb) {
          require('crypto').randomBytes(48, function (err, buf) {
            if (err) return cb(err)
            var token = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-')
            User.update({email: email}, {verification_token: token}).exec(cb)
          })
        },

        function (updated, cb) {
          var link = sails.config.connections.domain+'session/new_password?email='+email+'&token='+updated[0].verification_token
          Email.send({
            to: email,
            subject: 'Восстановление пароля',
            text: 'Для смены пароля вашей учетной записи перейдите по ссылке '+link,
            html: '<p>Для смены пароля вашей учетной записи перейдите по <a href="'+link+'">ссылке</a></p>'
          }, cb)
        }

      ]

      async.waterfall(fnArr, function (err) {
        if ( Er.default(req, res, err, 500) ) return
        req.flash('info', 'На указанную электронную почту было отправлено письмо с инструкциями по восстановлению пароля')
        res.redirect('/')
      })

      
    })
  },


  'new_password': function (req, res, next) {
    User.findOneByEmail(req.param('email')).exec(function (err, user) {
      if ( Er.default(req, res, err, 500) ) return
      if (!user) {
        req.flash('error', 'Пользователя с таким электронным адресом не зарегистрировано!')
        return res.redirect('/')
      }

      if (!user.verification_token || (user.verification_token != req.param('token')) ) {
        req.flash('error', 'Нехорошо обманывать!')
        return res.redirect('/')
      }

      res.view('session/passwordRecovery', {user: {email: user.email, verification_token: user.verification_token} } )
    })
  },


  'recovery_password': function (req, res, next) {
    User.findOneByEmail(req.param('email')).exec(function (err, user) {
      if ( Er.default(req, res, err, 500) ) return
      if (!user) {
        req.flash('error', 'Пользователя с таким электронным адресом не зарегистрировано!')
        return res.redirect('/')
      }

      if (!user.verification_token || (user.verification_token != req.param('verification_token')) ) {
        req.flash('error', 'Нехорошо обманывать!')
        return res.redirect('/')
      }

      if (!req.param('password') || req.param('password') != req.param('password_confirm')) {
        req.flash('warn', 'Пароль не совпадает с подтверждением')
        return res.redirect('back')
      }

      var bcrypt = require('bcryptjs')
        , fnArr = [
          function (cb) {
            bcrypt.genSalt(10, cb)
          },

          function (salt, cb) {
            bcrypt.hash(req.param('password'), salt, cb)
          }
        ]

      async.waterfall(fnArr, function (err, encryptedPassword) {
        if ( Er.default(req, res, err, 500) ) return

        user.encryptedPassword = encryptedPassword
        user.verification_token = null
        user.save(function (err) {
          if ( Er.default(req, res, err, 500) ) return
          req.flash('info', 'Смена пароля прошла успешно')
          res.redirect('/')
        })
        
      })
    })
  },
};

