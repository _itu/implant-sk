var gm = require('gm')
  , fs = require('fs')
  , path = require('path')

module.exports = {

  upload: function (req, res, next) {

    var type = req.param('type') || 'image'
      , name = req.param('ckeditor')? 'upload' : 'file'
      , uploadDir = path.join(sails.config.appPath,'uploads')
    
    req.file(name).upload({
      dirname: uploadDir
    },function (err, uploaded) {
      if ( Er.default(req, res, err, 500) ) return
      var tempArr = []

      async.each(uploaded, function (file, cb) {
        var ext = path.extname(file.filename)
          , base = path.basename(file.fd, ext)
          , temp = gm(file.fd).noProfile()
          , fnArr = [

            function (cb2) {
              temp.size(cb2)
            },

            function (size, cb2) {


              switch(type){
                case 'avatar': 
                  var x = (size.width >= 180)? 180 : null
                  tempArr.push('/uploads/'+base+'.jpg')
                  
                  if (!x) temp.background('white').gravity('Center').extent(180, size.height)
                  else temp.resize(180)

                  temp                  
                  .quality(80)
                  .write( path.join(uploadDir,base) + '.jpg', cb2)
                  break;

                case 'image':
                  var x = (size.width >= 1500)? 1500 : null
                    , y = (size.height >= 700)? 700 : null
                  tempArr.push('/uploads/'+base+'.jpg')

                  if (x || y) temp.resize(x,y)

                  temp
                  .quality(70)
                  .write( path.join(uploadDir,base) + '.jpg', cb2 )
                  break;

                case 'gallery':
                  var x = (size.width >= 1500)? 1500 : null
                    , y = (size.height >= 700)? 700 : null
                    , is_vertical = (size.width > size.height)? true : false
                  tempArr.push('/uploads/'+base+'_thumb.jpg')

                  async.parallel([

                    function (cb3) {
                      if (x || y) temp.resize(x,y)
                      temp.quality(70)
                      .write( path.join(uploadDir,base) + '.jpg', cb3 )
                    },

                    function (cb3) {
                      gm(file.fd)
                      .noProfile()
                      .resize( is_vertical? null : 100 , is_vertical? 100 : null)
                      .crop(100, 100)
                      .quality(70)
                      .write( path.join(uploadDir,base) + '_thumb.jpg', cb3 )
                    },

                    function (cb3) {
                      gm(file.fd)
                      .noProfile()
                      .resize(250)
                      .crop(250, 150)
                      .quality(70)
                      .write( path.join(uploadDir,base) + '_preview.jpg', cb3 )
                    }

                  ], function (err) {
                    if (err) return cb2(err)
                    cb2()
                  })
                  
                  break;
              }
            }
          ]

        async.waterfall(fnArr, function (err) {
          if (err) return cb(err)
          cb()
        })


      }, function (err) {
        if ( Er.default(req, res, err, 500) ) return
        async.each(uploaded, function (file, cb) {
          if ( file.type != 'image/jpeg' ) return fs.unlink(file.fd, cb);
          cb()
        }, function (err) {
          if ( Er.default(req, res, err, 500) ) return
          res.json( (tempArr.length === 1? tempArr[0] : tempArr) )
        })
      })
    })

  }

}