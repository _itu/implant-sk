/**
 * MessageController
 *
 * @description :: Server-side logic for managing messages
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'create': function (req, res, next) {
    Message.create( req.params.all() ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Message created')

      if (req.xhr) {

        var funcArr = [

          function (cb) {
            Dialog.findOrCreate({
              user_id: req.param('sender_id'),
              partner_id: req.param('recipient_id')
            },{
              user_id: req.param('sender_id'),
              partner_id: req.param('recipient_id'),
              last_message_id: created.id
            }).exec(function (err, dialog) {
              if (err) return cb(err)
              cb()
            })
          },

          function (cb) {
            Dialog.findOrCreate({
              user_id: req.param('recipient_id'),
              partner_id: req.param('sender_id')
            },{
              user_id: req.param('recipient_id'),
              partner_id: req.param('sender_id'),
              last_message_id: created.id
            }).exec(function (err, dialog) {
              if (err) return cb(err)
              cb()
            })
          }

        ]

        async.parallel(funcArr, function (err) {
          if ( Er.default(req, res, err, 500) ) return
          res.ok()
        })
      }
      else res.redirect('back')
    })
  },


  'delete': function (req, res, next) {
    Message.destroy( req.params.id ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Message ' + req.params.id + ' deleted');
      res.redirect('back')
    })
  },

};

