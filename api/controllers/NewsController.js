/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'list': function (req, res, next) {

    var page_num = req.params.page? req.params.page : 1,
        ITEMS_PER_PAGE = 12;

    Post
      .find({type: 'news'})
      .sort('createdAt DESC')
      .skip(ITEMS_PER_PAGE*(page_num-1))
      .limit(ITEMS_PER_PAGE).exec(function (err, newses) {
      if ( Er.default(req, res, err, 500) ) return
      if (!newses[0]) return next();

      res.view('post', { newses: newses } );
    })
  },
  

  'item': function (req, res, next) {
    Post.findOne( req.params.id ).exec(function (err, post) {
      if ( Er.default(req, res, err, 500) ) return
      if (!post) {
        sails.log.warn('Post not found')
        return next()
      };

      res.view('post/item', { post: post } );
    })
  },


  'create': function (req, res, next) {
    Post.create( req.params.all() ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Post created')
      res.redirect('/post/' + created.id)
    })
  },



  'edit': function (req, res, next) {
    Post.findOne( req.params.id ).exec(function (err, post) {
      if ( Er.default(req, res, err, 500) ) return
      if (!post) {
        sails.log.warn('Post not found')
        return next()
      };

      res.view('post/edit', { post: post } );
    })    
  },

};

