/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'list': function (req, res, next) {

    var page_num = req.param('page') || 1,
        ITEMS_PER_PAGE = 12;

    var funcArr = [

        function (cb) {
          Post.find({type: 'post'})
          .sort('createdAt DESC')
          .skip(ITEMS_PER_PAGE*(page_num-1))
          .limit(ITEMS_PER_PAGE)
          .populate('author_id')
          .populate('tags')
          .exec(function (err, posts) {
            if (err) return cb(err);
            if (!posts[0]) return cb(null, []);
            cb(null, posts)
          })
        },

        function (cb) {
          Post.count({type: 'post'}).exec(cb)
        }

      ]

    async.parallel(funcArr, function (err, results) {
      if ( Er.default(req, res, err, 500) ) return
      if (req.session.user) {

        async.each( results[0], function (post, cb) {
          
          Subscribe_to_post.findOne({ 
            user_id: req.session.user.id, 
            post_id: post.id 
          }).exec(function (err, subscribe) {
            if ( Er.default(req, res, err, 500) ) return
            
            if (!subscribe) {
              post.is_subscribed = false
              cb()
            } else {
              post.is_subscribed = true
              cb()
            }
          })

        }, function (err) {
          if ( Er.default(req, res, err, 500) ) return
          res.view('post', { posts: results[0], paginate: {items_count: results[1], items_per_page: ITEMS_PER_PAGE, page_num: page_num} } );
        })

      } else res.view('post', { posts: results[0], paginate: {items_count: results[1], items_per_page: ITEMS_PER_PAGE, page_num: page_num} } );

    })


    
  },
  

  'item': function (req, res, next) {

    var id = req.params.id
      , funcArr = [

        function (cb) {
          Post.findOne({id: id })
          .populate('author_id')
          .populate('tags')
          .exec(function (err, post) {
            if (err) return cb(err);
            cb(null, post)
          })
        },

        function (cb) {
          Comment.find( {post_id: id} )
          .populate('user_id')
          .exec(function (err, comments) {
            if (err) return cb(err);
            cb(null, comments)
          })

        }
      ]


    if (req.session.user) { 
      funcArr.push(function (cb) {
        Subscribe_to_post.findOne({ 
          user_id: req.session.user.id, 
          post_id: id 
        }).exec(function (err, subscribe) {
          if (err) return cb(err)
          if (!subscribe) cb(null, false);
          else {
            Subscribe_to_post.update_count_comments({ id: subscribe.id, post_id: id }, 
              function (err) {
                if (err) return cb(err)
                cb(null, true)
              })
          }
        })
      })
    }


    async.parallel(funcArr, function (err, result) {
      if ( Er.default(req, res, err, 500) ) return
      if (!result[0]) {
        sails.log.error('Post not found')
        return res.notFound()
      };

      if (result[2]) result[0].is_subscribed = result[2]

      res.view('post/item', { post: result[0], comments: result[1] } );
    })



  },


  'new': function (req, res, next) {
    res.view('post/editOrCreate', {action: 'create', post: {}} )
  },


  'edit': function (req, res, next) {
    Post.findOneById(req.param('id'))
    .populate('tags')
    .exec(function (err, post) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('post/editOrCreate', {action: 'edit', post: post} )
    })
  },


  'create': function (req, res, next) {
    var params = req.params.all()
    params.type = 'post'

    var fnArr = [
        function (cb) {
          if (!req.session.user.company_id) return cb();
          Company.query('SELECT our_tag FROM company WHERE id='+req.session.user.company_id
          , function (err, obj) {
            if (err) return cb(err)
            if ( !_.isEmpty(obj) ) { 
              params.tags = !params.tags
                ? obj[0].our_tag
                : (_.isArray(params.tags))
                  ? params.tags.concat(obj[0].our_tag)
                  : [params.tags ,obj[0].our_tag]
            }
            cb()
          })
        },

        function  (cb) {
          Post.create( params ).exec(cb)
        }
      ]


    async.series(fnArr, function (err, results) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Post ' + results[1].id + ' created by '+ req.session.user.id);
      res.redirect('/post/' + results[1].id)
    })
  },


  'update': function (req, res, next) {

    var id = req.param('id')
      , params = req.params.all()


    Post.findOne( id )
    .exec(function (err, post) {
      if ( Er.default(req, res, err, 500) ) return
      if (!post) {
        sails.log.warn('Post not found for updating');
        return res.redirect('/post');
      }

      params.tags || (params.tags = [])
      Post.update( {id: id}, params ).exec(function (err, updated) {
        if ( Er.default(req, res, err, 500) ) return
        sails.log.info('Post ' + req.param('id') + ' updated by '+ req.session.user.id);
        res.redirect('/post/' + id)
      })
    })
  },


  'remove': function (req, res, next) {
    var post_id = req.param('id')
      , fnArr = [

        function (cb) {
          Post.destroy( post_id ).exec(cb)
        },

        function (cb) {
          User.query('DELETE FROM subscribe_to_post WHERE post_id='+post_id, cb)
        }

      ]

    async.parallel(fnArr, function (err) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Post ' + post_id + ' deleted by '+ req.session.user.id);
      res.redirect('/post')
    })
    
  },

};

