module.exports = {

  'item': function (req, res, next) {

    var tag_id = req.param('id')
      , locale = req.getLocale()
      , fnArr = [

        function  (cb) {
          Tag.findOneById(tag_id).populate('with_events').exec(cb)
        },

        function (cb) {
          User.query(
            "SELECT "+
              "post.title_"+locale+" as title_"+locale+", "+
              "post_tag.id as id, "+
              "CONCAT('[', "+
                "GROUP_CONCAT(CONCAT('{\"name_"+locale+"\":\"', tags.name_"+locale+", '\", \"id\":\"', tags.id,'\"}')), "+
              "']') tags "+
            "FROM tag "+
            "LEFT JOIN post_tags__tag_with_posts p_t ON p_t.tag_with_posts=tag.id "+
            "LEFT JOIN post ON post.id=p_t.post_tags "+
            "LEFT JOIN post_tags__tag_with_posts post_tag ON post_tag.post_tags=post.id "+
            "LEFT JOIN tag as tags ON tags.id=post_tag.tag_with_posts "+
            "WHERE tag.id="+tag_id+" "+
            "GROUP BY post.id"
          , cb)
        },

        function (cb) {
          Knowledge.findOne({tag_id: tag_id}).exec(cb)
        },

        function (cb) {
          if (!req.session.user) return cb()
          User.query('SELECT id FROM tag_with_users__user_interests WHERE user_interests='+req.session.user.id+' AND tag_with_users='+req.param('id'), cb)
        },

        function (cb) {
          User.query('SELECT tag.id, tag.name_'+locale+', post_tags__tag_with_posts.post_tags FROM tag LEFT JOIN post_tags__tag_with_posts ON post_tags__tag_with_posts.tag_with_posts=tag.id WHERE tag.id=2', cb)
        }
      ]

    async.parallel(fnArr, function (err, results) {
      if ( Er.default(req, res, err, 500) ) return
      if (!results[0]) return res.notFound()

      results[0].posts = results[1]
      if (results[2]) results[0].knowledge_id = results[2].id
      if (!_.isEmpty(results[3])) results[0].in_interests = true
      res.view('tag/item', {tag: results[0]})
    })
  },


  'new': function (req, res, next) {
    res.view('tag/editOrCreate', {action: 'create', tag: {}} )
  },


  'edit': function (req, res, next) {
    Tag.findOneById(req.param('id'))
    .exec(function (err, tag) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('tag/editOrCreate', {action: 'edit', tag: tag} )
    })
  },


  'create': function (req, res, next) {
    Tag.create(req.params.all()).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Tag ' + created.id + ' created by ' + req.session.user.id);
      res.redirect('/tag/'+created.id)
    })
  },


  'update': function (req, res, next) {
    Tag.update({id: req.param('id')}, req.params.all()).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Tag ' + req.param('id') + ' updated by ' + req.session.user.id);
      res.redirect('/tag/'+req.param('id'))
    })
  },


  'remove': function (req, res, next) {
    Tag.destroy( {id: req.param('id')} ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Tag ' + req.param('id') + ' deleted by ' + req.session.user.id);
      res.redirect('/event')
    })
  },

}