var util = require('util')

module.exports = {

  'test': function (req, res, next) {
    sails.log.debug( util.inspect(req.params.all(), {depth: null}) );
    res.redirect('back')
  },



  'user_list': function (req, res, next) {
    User.query('SELECT id, first_name, second_name, last_name FROM user', function (err, users) {
      if ( Er.default(req, res, err, 500) ) return
      users.forEach(function (user) {
        user.name = user.last_name + ' ' + user.first_name + (user.second_name? ' '+user.second_name : '') 
      })

      res.json(users);
    })
  },


  'tag_list' : function (req, res, next) {

    var company_id = req.session.user.company_id
      , locale = req.getLocale()
      , fnArr = [

        function (cb) {
          if (!company_id) return cb(null, null)
          Company.query('SELECT our_tag FROM company WHERE id='+company_id, cb)
        },

        function (arr, cb) {
          var allowed_tag = _.isEmpty(arr)? '' : ' OR id='+arr[0].our_tag
          Tag.query("SELECT tag.id as id, tag.name_"+locale+" as name FROM tag WHERE name_"+locale+" NOT LIKE '"+sails.__({phrase: 'company_blog', locale: locale})+"%'"+allowed_tag, cb)
        }

      ]

    async.waterfall(fnArr, function (err, tags) {
      if ( Er.default(req, res, err, 500) ) return
      res.json(tags)
    })
  },


  'company_list' : function (req, res, next) {
    Tag.query('SELECT id, name_'+req.getLocale()+' as name FROM company', function (err, companies) {
      if ( Er.default(req, res, err, 500) ) return
      res.json(companies)
    })
  },

}