module.exports = {
	

  'list': function (req, res, next) {

    var opts = {}
      , locale = req.getLocale()

    if (req.param('type')) opts.type = req.param('type')
    if (req.param('text')) opts.name = {'contains': req.param('text')}

    Knowledge.find(opts)
      .sort('name_'+locale+' ASC')
      .exec(function (err, knowledges) {
      if ( Er.default(req, res, err, 500) ) return
      if (knowledges[0]) {
        var listing = {}, firstLetter
        for (var i = 0; i < knowledges.length; i++) {
          firstLetter = knowledges[i]['name_'+locale].charAt(0).toUpperCase()
          if ( listing[firstLetter] ) listing[firstLetter]++
          else listing[firstLetter] = 1
        };
      };

      res.view('knowledge', { knowledges: knowledges, listing: listing } );
    })
  },
  

  'item': function (req, res, next) {
    Knowledge.findOne( req.param('id') ).exec(function (err, knowledge) {
      if ( Er.default(req, res, err, 500) ) return
      if (!knowledge) {
        sails.log.warn('Knowledge '+req.param('id')+' not found')
        return next()
      };

      res.view('knowledge/item', { knowledge: knowledge } );
    })
  },


  'new': function (req, res, next) {
    res.view('knowledge/editOrCreate', {action: 'create', knowledge: {}} )
  },


  'edit': function (req, res, next) {
    Knowledge.findOneById(req.param('id'))
    .exec(function (err, knowledge) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('knowledge/editOrCreate', {action: 'edit', knowledge: knowledge} )
    })
  },


  'create': function (req, res, next) {
    Knowledge.create( req.params.all() ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      res.redirect('/knowledge/' + created.id)
    })
  },

  'update': function (req, res, next) {
    Knowledge.update( {id: req.param('id')}, req.params.all() ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      res.redirect('/knowledge/' + req.param('id'))
    })
  },

  'delete': function (req, res, next) {
    Knowledge.destroy( req.param('id') ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      res.redirect('/knowledge')
    })
  },


};

