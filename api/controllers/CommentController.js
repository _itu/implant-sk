/**
 * CommentController
 *
 * @description :: Server-side logic for managing companies
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'create': function (req, res, next) {
    Comment.create( req.params.all() ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Comment created')
      res.redirect('back')
    })
  },


  'update': function (req, res, next) {

    Comment.findOneById( req.param('id') ).exec(function (err, comment) {
      if ( Er.default(req, res, err, 500) ) return
      if (!comment) {
        sails.log.warn('User not found for updating');
        return next();
      }

      Comment.update( { id: req.param('id') }, {html: req.param('html')} ).exec(function (err, updated) {
        if ( Er.default(req, res, err, 500) ) return
        sails.log.info('Comment ' + req.param('id') + ' updated');
        res.redirect('back')
      })

    })

  },

};

