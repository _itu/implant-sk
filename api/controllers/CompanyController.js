/**
 * CompanyController
 *
 * @description :: Server-side logic for managing companies
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  'redirect': function (req, res, next) {
    res.redirect('/company')
  },

  'item': function (req, res, next) {

    // var id = req.rapam('id')
    var id = 1
      , funcArr = [

        function (cb) {
          Company.findOneById(id).exec(function (err, company) {
            if (err) return cb(err)
            if (_.isEmpty(company)) return cb({err: 'Компании не найдено'});
            cb(null, company)
          })
        },

        function (cb) {
          Contact.getCompanyContacts(id, cb)
        }
      ]
    
    async.parallel(funcArr, function (err, results) {
      if ( Er.default(req, res, err, 500) ) return

      var company = results[0]
      company.contact_id = results[1]
      // тут можно или сделать слияние контактов с объектом компании или оставить раздельно
      res.view('company/item',{company: company})
    })

  },


  'new': function (req, res, next) {
    res.view('company/editOrCreate', {action: 'create', company: {}} )
  },

  
  'edit': function (req, res, next) {
    // var company_id = req.param('id')
    var company_id = 1

    Company.findOneById(company_id)
    .populate('employee')
    .exec(function (err, company) {
      if ( Er.default(req, res, err, 500) ) return

      Contact.getCompanyContacts(company_id, function (err, contacts) {
        if ( Er.default(req, res, err, 500) ) return
        
        company.contacts = contacts        
        res.view('company/editOrCreate', {action: 'edit', company: company} )
      })
    })
  },


  'update': function (req, res, next) {
    // var company_id = req.param('id')
    var params = req.params.all()
      , schema = {
      model: 'company',
      not_removed: true,
      links: [
        {
          model: 'contact',
          by: 'contacts',
          via: 'company_id',
          links: [
            {
              model: 'contact_field',
              by: 'fields',
              via: 'contact_id'
            }
          ]
        }
      ] 
    }

    params.employee || (params.employee = [])
    Database.new_update(params, schema, [], function (err) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/company')
      // res.redirect('/company/'+company_id)
    })
  },


  
  'events': function (req, res, next) {
    
    var company_id = 1
      , fnArr = [

        function (cb) {
          Company.findOneById(company_id)
          .exec(function (err, company) {
            if (err) return cb(err)
            cb(null, company)
          })
        },

        function (cb) {
          Participants_of_event.find({
            company_id: company_id,
            role: 'organizer'
          }).populate('event_id')
          .exec(function (err, events) {
            if (err) return cb(err)
            cb(null, _.pluck(events, 'event_id'))
          })
        }

      ]
    
    
    async.parallel(fnArr, function (err, results) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('company/item', {company: results[0], events: results[1]} )
    })


  },


  'published': function (req, res, next) {
    
    var company_id = 1
      , funcArr = [

        function (cb) {
          Company.findOneById(company_id)
          .populate('our_tag').exec(function (err, company) {
            if (err) return cb(err)
            cb(null, company)
          })
        },

        function (company, cb) {
          if (_.isEmpty(company.our_tag)) return cb(null, company, [])

          // Tag.findOneById(company.our_tag.id).populate('with_posts')
          // .exec(function (err, tag) {
          Tag.query('SELECT post_tags FROM post_tags__tag_with_posts WHERE tag_with_posts='+company.our_tag.id, function (err, posts_id) {
            if (err) return cb(err)
            if (!posts_id[0]) return cb(null, company, [])

            var postsIdArr = _.pluck(posts_id, 'post_tags')
            Post.find({id: postsIdArr})
            .populate('author_id')
            .populate('tags')
            .exec(function (err, posts) {
              if (err) return cb(err)
              cb(null, company, posts)
            })
          })
        }
      ] 

    async.waterfall(funcArr, function (err, company, posts) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('company/item', {company: company, published: posts})
    })

  },


  'employee': function (req, res, next) {
    
    var company_id = 1
    
    Company.findOneById(company_id)
    .populate('employee')
    .exec(function (err, company) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('company/item', {company: company, employee: company.employee} )
    })

  },
	
};

