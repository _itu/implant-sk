/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  'item': function (req, res, next) {
    if (isNaN(parseInt(req.param('id')))) {
      sails.log.warn('User ID must be integer')
      return next()
    }
    
    User.findOne( req.param('id') )
    .populate('educations')
    .exec(function (err, user) {
      if ( Er.default(req, res, err, 500) ) return
      if (!user) {
        sails.log.warn('User not found')
        return next()
      };


      var fnArr = [
        function (cb) {
          Contact.getContact(user.contact_id, cb)
        },

        function (cb) {
          if (!req.session.user || !(req.session.user.id != user.id)) return cb()
          User.is_friends(req.session.user.id, user.id, cb)
        }
      ]


      async.parallel(fnArr, function (err, results) {
        if ( Er.default(req, res, err, 500) ) return
        user = user.toJSON()
        user.contact_id = results[0]
        if (results[1]) user.is_friend = results[1]

        res.view('user/item', { user: user } )
      })
    })
  },


  'edit': function (req, res, next) {
    User.findOneById(req.param('id') )
    .populate('educations')
    .exec(function (err, user) {
      if ( Er.default(req, res, err, 500) ) return
      var fnArr = [
        function (cb) {
          Contact.getContact(user.contact_id, cb)
        },

        function (cb) {
          if (!req.session.user || !(req.session.user.id != user.id)) return cb()
          User.is_friends(req.session.user.id, user.id, cb)
        }
      ]


      async.parallel(fnArr, function (err, results) {
        if ( Er.default(req, res, err, 500) ) return
        user = user.toJSON()
        user.contact = results[0]
        if (results[1]) user.is_friend = results[1]

        res.view('user/editOrCreate', { action: 'edit', user: user } )
      })

    })
  },


  'create': function (req, res, next) {

    if (req.param('password') != req.param('password_confirm')) {
      req.flash('error', 'Пароли не совпадают')
      return res.redirect('back')
    }

    var funcArr = [

      function (cb) {
        Contact.create().exec(function (err, created) {
          if (err) {
            if (err.ValidationError) Transform.validationError_to_flash(Contact, err.ValidationError, req)
            return cb(err)
          }

          cb(null, created)
        })
      },

      function (contact, cb) {
        require('crypto').randomBytes(48, function (err, buf) {
          if (err) return cb(err)
          var token = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-')
          cb(null, contact, token)
        })
      },

      function (contact, token, cb) {
        var params = _.assign({contact_id: contact.id, verification_token: token}, req.params.all())
        console.log(params);
        User.create( params ).exec(function (userErr, created) {
          if (userErr) {
            console.log('user error!');
            if (userErr.ValidationError) {
              console.log('user validate error!');
              Transform.validationError_to_flash(User, userErr.ValidationError, req)
            }
            Contact.destroy({id: contact.id}).exec(function (contactErr) {
              console.log('contact destroyed');
              if (contactErr) return cb(_.assign(userErr,contactErr))
              return cb(userErr)
            })
          }

          console.log(created);

          cb(null, created)
        })
      }
    ]

    async.waterfall(funcArr, function (err, user) {
      if ( Er.default(req, res, err, 500) ) return
      var link = sails.config.connections.domain+'session/confirm?email='+user.email+'&token='+user.verification_token

      Email.send({
        to: req.param('email'),
        subject: 'Подтверждение регистрации',
        text: 'Если вы не имеете понятия для чего это письмо, пожалуйста проигнорируйте его. Для подтверждения вашей электронной почты пожалуйста перейдите по ссылке '+link,
        html: '<p>Если вы не имеете понятия для чего это письмо, пожалуйста проигнорируйте его.</p><p>Для подтверждения вашей электронной почты пожалуйста перейдите по <a href="'+link+'">ссылке</a></p>'
      }, function (err, info) {
        if ( Er.default(req, res, err, 500) ) return
        req.flash('info', 'Для завершения регистрации подтвердите свой email. Письмо должно прийти к вам на почту в течении 5 минут.')
        res.redirect('back')
      })
    })

    
  },


  'update': function (req, res, next) {

    var user_id = req.param('id')
      , params = req.params.all()
      , schema = {
      model: 'user',
      not_removed: true,
      links: [
        {
          model: 'contact',
          by: 'contact',
          linked_by: 'contact_id',
          links: [
            {
              model: 'contact_field',
              by: 'fields',
              via: 'contact_id'
            }
          ]
        },
        {
          model: 'education',
          by: 'educations',
          via: 'user_id'
        }
      ] 
    }

    Database.new_update(params, schema, [], function (err) {
      if ( Er.default(req, res, err, 500) ) return
      delete params.type
      delete params.display_name
      Tools.clear_empty_values(params)

      if (req.session.user.id == user_id) {
        _.assign(req.session.user, params)
        res.cookie('lang', req.session.user.locale)
      }
      res.redirect('/user/'+user_id)
    })
  },


  // 'change_password': function (req, res, next) {

  //   User.findOneByEmail(req.param('email')).exec(function (err, user) {
  //     if (err) {
  //       sails.log.error(err)
  //       return next(err)
  //     }
      
  //     if (req.param('token') != user.verification_token) {};
  //   })
  // },


  'dialogs': function (req, res, next) {

    Dialog.find({ user_id: req.param('id') })
    .sort('updatedAt DESC')
    .populate('partner_id')
    .populate('last_message_id')
    .exec(function (err, dialogs) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('user/item', {user: req.session.user ,dialogs: dialogs})
    })
  },


  'messages': function (req, res, next) {

    var user_id = req.param('id')
      , partner_id = req.param('partner')
      , funcs = [

      function (cb) {
        User.findOneById( partner_id ).exec(function (err, partner) {
          if (err) return cb(err)
          if (!partner) return cb({err: 'user not found'})

          cb(null, partner);
        })
      },

      function (cb) {
        Message.find({ or: [
          {sender_id: user_id, recipient_id: partner_id},
          {sender_id: partner_id, recipient_id: user_id}
        ]})
        .sort('id DESC')
        .exec(function (err, messages) {
          if (err) return cb(err)

          async.each(
            messages.filter(function(message) {
              return !message.is_received && message.sender_id == partner_id
            }),

            function (message, cb2) {
              message.is_received = true;
              message.save(cb2)
            },

            function (err) {
              if (err) cb(err)
              cb(null, messages)
            }
          )
        })
      }
    ]

    async.parallel(funcs, function (err, values) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('user/item', {user: req.session.user, partner: values[0], messages: values[1]})
    })
    
  },


  'published': function (req, res, next) {

    Post.find({author_id: req.param('id')})
    .populate('tags')
    .exec(function (err, posts) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('user/item', {user: req.session.user, published: posts})
    })
    
  },


  'favorites': function (req, res, next) {

    var user_id = req.param('id')
      , funcArr = [

        function (cb) {
          
          User.findOneById(user_id)
          .populate('interests')
          .populate('favorite_users')
          .populate('favorite_events')
          .exec(function (err, user) {
            if (err) return cb(err)
            cb(null, {
              interests: user.interests,
              events: user.favorite_events,
              users: user.favorite_users
            })
          })

        },

        function (cb) {
          
          Subscribe_to_post.find({ user_id: user_id })
          .populate('post_id')
          .exec(function (err, subscribes) {
            if (err) return cb(err)
            if (_.isEmpty(subscribes)) return cb(null, {posts: []})

            var postsId = []
            for (var i = 0; i < subscribes.length; i++) {
              postsId.push(subscribes[i].post_id.id)
            };

            Post.find({id: postsId})
            .populate('author_id')
            .populate('tags')
            .exec(function (err, posts) {
              if (err) return cb(err)
              cb(null, {posts: posts})
              
            })
          })

        }

      ]

    async.parallel(funcArr, function (err, results) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('user/item', { user: req.session.user, favorites: _.assign(results[0], results[1]) })
    })

    
  },


  // ajax request

  'addFavoriteUser': function (req, res, next) {
    Tools.userFavs('add', req.param('id'), 'favorite_users', req.param('friend_id'), res, next)
  },


  'removeFavoriteUser': function (req, res, next) {
    Tools.userFavs('remove', req.param('id'), 'favorite_users', req.param('friend_id'), res, next)
  },

  'addFavoriteTag': function (req, res, next) {
    Tools.userFavs('add', req.param('id'), 'interests', req.param('tag_id'), res, next)
  },


  'removeFavoriteTag': function (req, res, next) {
    Tools.userFavs('remove', req.param('id'), 'interests', req.param('tag_id'), res, next)
  },


  'addFavoriteEvent': function (req, res, next) {
    Tools.userFavs('add', req.param('id'), 'favorite_events', req.param('event_id'), res, next)
  },


  'removeFavoriteEvent': function (req, res, next) {
    Tools.userFavs('remove', req.param('id'), 'favorite_events', req.param('event_id'), res, next)
  },


  'addFavoritePost': function (req, res, next) {
    Subscribe_to_post.create({
      user_id: req.param('id'),
      post_id: req.param('post_id'),
      now_count_comments: req.param('countComments')
    }).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      res.ok()
    })
  },


  'removeFavoritePost': function (req, res, next) {
    Subscribe_to_post.destroy({
      user_id: req.param('id'),
      post_id: req.param('post_id')
    }).exec(function (err, destroyed) {
      if ( Er.default(req, res, err, 500) ) return
      res.ok()
    })
  },


  'enrollToEvent': function (req, res, next) {
    var params = {
      user_id: req.param('id'),
      event_id: req.param('event_id'),
    }

    if (req.param('phone')) params.phone = req.param('phone')

    Going_to_event.create(params).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      User.query("SELECT event.name, event.slug, contact_field.value as email "+
                 "FROM event "+
                 "LEFT JOIN contact ON contact.id=event.contact_id "+
                 "LEFT JOIN contact_field ON contact_field.contact_id=contact.id "+
                 "WHERE contact_field.type='email' AND event.id="+req.param('event_id')
      , function (err, event) {
        if (err) {
          sails.log.error(err)
          req.flash('info', sails.__({phrase: 'successful_enroll', locale: req.getLocale()}))
          return res.redirect('back')
        }

        var email = event[0].email || 'dolgalev@dolgalev-sk.ru'
          , emailParams = {
              to: email,
              subject: 'Пользователь записался на мероприятие',
              text: 'Пользователь '+req.session.user.display_name+' (id - '+req.session.user.id+( req.param('phone')? ', контактый номер - '+req.param('phone') : '' )+'), записался на мероприятие - '+event[0].name+' (ссылка - http://implantat-sk.ru/event/'+event[0].slug+').',
              html: '<p>Пользователь '+req.session.user.display_name+' (id - '+req.session.user.id+( req.param('phone')? ', контактый номер - '+req.param('phone') : '' )+'), записался на мероприятие - <a href="/event/'+event[0].slug+'">'+event[0].name+'</a>.</p>'
            }
          
        Email.send(emailParams, function (err) {
          req.flash('info', sails.__({phrase: 'successful_enroll', locale: req.getLocale()}))
          res.redirect('back')
        })

        
      })

      
    })
  },


  'ajaxUpdate': function (req, res, next) {
    var params = req.params.all()
    for (var key in params) if (key.search('is_') == -1) delete params[key]

    User.update({id: req.param('id')}, params ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      res.ok()
    })

  }


  // 'feed'

};

