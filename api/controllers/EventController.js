/**
 * EventController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  'list': function (req, res, next) {

    var page_num = req.param('page') || 1
      , ITEMS_PER_PAGE = 16
      , locale = req.getLocale()

    var funcArr = [

      function (cb) {
        Event.query('SELECT event.slug, event.name, event.date, event.type, event.ticket_price, event.lang, event.duration, contact.city, event_day.day_start as event_start '
                  +'FROM event '
                  +'LEFT JOIN contact ON event.contact_id=contact.id '
                  +'LEFT JOIN event_day ON event.id=event_day.event_id AND day_number=1 '
                  +'WHERE event.date>now() '
                  +'ORDER BY event.date ASC'
        , function (err, events) {
          if (err) return cb(err)
          if (!events[0]) return cb(null, [])
          cb(null, events)
        })
      },

      function (cb) {
        Event.count({ date: {'>': new Date()} }).exec(function (err, count) {
          if (err) return cb(err)
          cb(null, count)
        })
      }

    ]

    async.parallel(funcArr, function (err, results) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('event', { events: results[0], paginate: {items_count: results[1], items_per_page: ITEMS_PER_PAGE, page_num: page_num} } );
    })
  },
	

  'item': function (req, res, next) {
    Event.getEvent( req.param('slug'), function (err, event) {
      if ( Er.default(req, res, err, 500) ) return
      if (req.session.user) {

        var fnArr = [
          function (cb) {
            User.query('SELECT id FROM event_subscribers__user_favorite_events WHERE event_subscribers='+event.id+' AND user_favorite_events='+req.session.user.id, cb)
          },

          function (cb) {
            Going_to_event.findOne({user_id: req.session.user.id, event_id: event.id}).exec(cb)
          }
        ]

        async.parallel(fnArr, function (err, results) {
          if ( Er.default(req, res, err, 500) ) return
          if (!_.isEmpty(results[0])) event.is_subscribed = true
          if (!_.isEmpty(results[1])) {
            event.is_enroll = true
            if (results[1].is_went) event.is_went = true;
          }
          res.view('event/item', {event: event})
        })

      } else res.view('event/item', {event: event})

    } )
  },


  'new': function (req, res, next) {
    res.view('event/editOrCreate', {action: 'create', event: {}} )
  },


  'enrolledList': function (req, res, next) {

    var authorFilter = (req.session.user.type === 'administrator' || req.session.user.type === 'moderator')? '' : ' WHERE event.author_id='+req.session.user.id+' '

    Event.query('SELECT event.name, event.id,  COUNT(going_to_event.event_id) as enrolled, COUNT(IF(going_to_event.is_handled=0, 1, NULL)) as not_handled '+
                'FROM going_to_event '+
                'LEFt JOIN event ON event.id=going_to_event.event_id '+
                authorFilter+
                'GROUP BY event_id '+
                'ORDER BY not_handled DESC'
    , function (err, events) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('event/enrolledList', {events: events})
    })

  },


  'enrolled': function (req, res, next) {
    Going_to_event.find( {event_id: req.param('id')} )
    .populate('user_id')
    .exec(function (err, enroll) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('event/enrolled', {enroll: enroll})
    })
  },


  'edit': function (req, res, next) {
    Event.getEvent(req.param('slug'), function (err, event) {
      if ( Er.default(req, res, err, 500) ) return
      res.view('event/editOrCreate', {action: 'edit', event: event} )
    })
  },


  'create': function (req, res, next) {

    var params = req.params.all()
      , schema = {
          model: 'event',
          not_removed: true,
          links: [
            {
              model: 'contact',
              linked_by: 'contact_id',
              by: 'contacts',
              links: [
                {
                  model: 'contact_field',
                  by: 'fields',
                  via: 'contact_id'
                }
              ]
            },
            {
              model: 'event_day',
              by: 'event_days',
              via: 'event_id',
              links: [
                {
                  model: 'event_part',
                  by: 'parts',
                  via: 'event_day_id'
                }
              ] 
            },
            {
              model: 'participants_of_event',
              via: 'event_id',
              by: 'participants'
            }
          ] 
        }
      , fnArr = [
        function (cb) {
          Database.new_update(params, schema, [], cb)
        }
      ]

    if ( !params.contacts.fields.some(function (item) {return item.type === 'latlng'})
        && params.contacts.fields.some(function (item) {return item.type === 'address'}) ) {
      fnArr.unshift( function (cb) {
        require('http').get('http://maps.googleapis.com/maps/api/geocode/json?language='+req.getLocale()+'&address='+params.contacts.city+', '+_.find( params.contacts.fields, {type: 'address'}).value, function (mapRes) {
          if (mapRes.statusCode != 200) return cb()
          var temp = ''
          mapRes.on('data', function (data) {
            temp += data.toString()
          })

          mapRes.on('end', function () {
            temp = JSON.parse(temp)
            if (temp.status != 'OK') return cb();

            params.contacts.fields.push({
              contact_id:  params.contacts.id,
              type: 'latlng',
              value: temp.results[0].geometry.location.lat+','+temp.results[0].geometry.location.lng
            })
            cb()
          })
          
        }).on('error', function (err) {
          cb(err)
        })
      } )
    }

    async.waterfall(fnArr, function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      Event.query('SELECT slug FROM event WHERE id='+created, function (err, result) {
        res.redirect('/event/'+result[0].slug)
      })
    })

  },


  'update': function (req, res, next) {

    var params = req.params.all()
      , event_slug = req.param('slug')
      , schema = {
          model: 'event',
          not_removed: true,
          links: [
            {
              model: 'contact',
              linked_by: 'contact_id',
              by: 'contacts',
              links: [
                {
                  model: 'contact_field',
                  by: 'fields',
                  via: 'contact_id'
                }
              ]
            },
            {
              model: 'event_day',
              by: 'event_days',
              via: 'event_id',
              links: [
                {
                  model: 'event_part',
                  by: 'parts',
                  via: 'event_day_id'
                }
              ] 
            },
            {
              model: 'participants_of_event',
              via: 'event_id',
              by: 'participants'
            }
          ] 
        }

    params.speakers || (params.speakers = [])
    Database.new_update(params, schema, [], function (err, slug) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Event ' + event_slug + ' updated by '+ req.session.user.id);
      res.redirect('/event/'+slug)
    })
  },


  'remove': function (req, res, next) {
    Event.destroy( {slug: req.param('slug')} ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      sails.log.info('Event ' + req.param('slug') + ' removed by '+ req.session.user.id);
      res.redirect('/event')
    })
  },


  'ajaxUpdate': function (req, res, next) {
    var params = req.params.all()
    for (var key in params) if (key.search('is_') == -1) delete params[key]

    Going_to_event.update({id: req.param('id')}, params ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      res.ok()
    })

  }

};

