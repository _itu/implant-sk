
;(function ( $, window, document, undefined ) {

  var pluginName = "tabulous",
    defaults = {
      effect: 'scale',
      target_class: 'tab-content'
    };

  function Plugin( element, options ) {
    this.element = element;
    this.$elem = $(this.element);
    this.options = $.extend( {}, defaults, options );
    this._defaults = defaults;
    this.init();
  }

  Plugin.prototype = {

    init: function() {

      var self = this
        , is_animate = false
        , links = this.$elem.find('.tabs-menu').find('.item')
        , container = this.$elem.find('#tabs_container')
        , tabs = container.children('[id]')
        , tab_content;

      tabs.css({'opacity': 0,'visibility': 'hidden'});
      tabs.css({'position':'absolute'}).first().addClass('tabs-active')
      container.css({'height': tabs.first().height(), 'opacity': '1'});

      links.on('click', function (event) {
        event.preventDefault();
        if ( $(this).hasClass('active') ) return;

        tabSwitch(this)
      });

      if (links.filter('.active')[0]) tabSwitch( links.filter('.active')[0] )
      else tabSwitch( links[0] )


      function tabSwitch(el) {

        if (is_animate) return;
        is_animate = true
       
        var $this = $(el)
          , id = $this.attr('href')
          , prevTab = tabs.filter('.tabs-active')
          , thisTab = container.find(id)

        links.removeClass('active');
        $this.addClass('active');
        prevTab.removeClass('tabs-active')
        thisTab.addClass('tabs-active')

        var tl = new TimelineMax()
        tl.to(prevTab, .3, {css: {scale: .8, autoAlpha: 0}})
          .to(container, .1, {css: {height: container.find(id).height()}})
          .fromTo(thisTab, .3, {css: {scale: .6, autoAlpha: 0}}, {css: {scale: 1, autoAlpha: 1}})
          .call(function () {is_animate = false})
      }

    }

  };

  $.fn[pluginName] = function ( options ) {
    return this.each(function () {
      new Plugin( this, options );
    });
  };

})( jQuery, window, document );
