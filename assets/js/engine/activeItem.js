(function ($) {
 
  $.fn.activeItem = function (options) {
 
    return this.each(function() {
      
      var $this = $(this);
          
      if (!$this.data('activeItem')) {
        $(this).data('activeItem', {
          target: $this,
          items: $this.find('.item'),
          active: $this.find('.active'),
          size: $this.data('ai-size') || '2px',
          type: $this.data('ai-type') || 'horizontal'
        });
      };

      var data = $this.data('activeItem')
        , options = {
          sizeType: data.type === 'horizontal'? 'height' : 'width',
          changingType: data.type === 'horizontal'? 'width' : 'height',
          moveDirection: data.type === 'horizontal'? 'left' : 'top',

        }

      data.target.append('<div style="'+ options.sizeType +': '+ data.size +'" class="marker"></div>')
 
      var marker = data.target.find('.marker')
        , timer;

      function valuesEngine (arr) {

        if (arr.length !== 4) {
          switch(arr.length){
            case 1:
              arr = arr.concat([arr[0],arr[0],arr[0]])
              break;
            case 2:
              arr = arr.concat(arr)
              break;
            case 3:
              arr = arr.concat(arr[1])
              break;
          }
        }

        return arr
      }

      function moveMarker (el) {

        clearTimeout(timer);
        var el = $(el)
          , pad = el.css('padding').replace(/px/g, '').split(' ')
          , mar = el.css('margin').replace(/px/g, '').split(' ')

        pad = valuesEngine(pad)
        mar = valuesEngine(mar)

        pad = data.type === 'horizontal'? [pad[1]>>0, pad[3]>>0] : [pad[0]>>0, pad[2]>>0]
        mar = data.type === 'horizontal'? [mar[1]>>0, mar[3]>>0] : [mar[0]>>0, mar[2]>>0]

        var markerSize = el['inner'+ options.changingType.charAt(0).toUpperCase() +options.changingType.slice(1)]() - (pad[0]+pad[1])
          , markerMoveDirection = el.position()[options.moveDirection] + pad[1] + (data.type === 'horizontal'? mar[1] : 0 )
          , cssOpts = {}

        cssOpts[options.changingType] = markerSize
        cssOpts[options.moveDirection] = markerMoveDirection

        TweenMax.to(marker, .3, {css: cssOpts })
      }

      function removeMarker () {
        if (data.active[0]) {
          moveMarker(data.active[0])
        } else {
          var cssOpts = {}
          cssOpts[options.changingType] = 0

          TweenMax.to(marker, .3, {css: cssOpts})
        }
      }


      if (data.active[0]) setTimeout(function () {
        moveMarker(data.active[0])
      }, 200);
      

      data.items.on('mouseenter', function(event) {
        moveMarker(this)
      });


      data.items.on('mouseleave', function(event) {
        clearTimeout(timer);
        timer = setTimeout(function () {
          removeMarker();
        }, 300);
      });


      data.items.on('click', function(event) {
        data.active = $(this)
      });


    });
  }
 
})(jQuery);