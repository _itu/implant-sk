var app;

(function ($) {

  var APP = function () {

    var self = this;

    self.vars = {
      body: $('body'),
      header: $('#header'),
      main: $('#main'),
      footer: $('#footer'),
      doc: $(document),
      win: $(window),
      overlay: $('#overlay'),
      popup: $('#popup'),
      auth: $('#Authentificated'),
      notification: $('#Notification'),
      csrf: $('body').data('csrf'),
      ckeditor_counter: 0
    }

    if (self.vars.auth[0]) self.vars.auth_id = self.vars.auth.data('id')

    self.fn = {

      showNotification: function (type, text) {
        var message = text? text : (type == 'error')? 'Ошибка сервера' : 'Выполено успешно'
          , item = $('<div class="item '+type+'">'+message+'</div>')
        self.vars.notification.append(item)

        setTimeout(function () {
          item.fadeOut(function() {item.remove()})
        }, 5000);

      },


      requestHandler: function ($el, triggeredClass, req, addExtendParams, additionalFuncs) {

        // $el - jquery объект по которому произошел клик
        // triggeredClass - переключаемый класс на кнопке по которому определяется add/remove
        // req:
        //  req.entity - оперируемая сущность для "избранного"
        //  req.entityName - отображаемое имя сущности (для уведомлений)
        // addExtendParams - объект расширяющий стандартный объект параметров содержащий _csrf токен
        // additionalFuncs - дополнительная логика для обработки удачного завершения

        if ($el.hasClass(triggeredClass)) {
          var action = 'remove'
            , funcIndex = 0
            , notificationText = (req.type === 'favorites')
              ? req.entityName + ' удален из избранного'
              : req.notification
        } else {
          var action = 'add'
            , funcIndex = 1
            , notificationText = (req.type === 'favorites')
              ? req.entityName + ' добавлен в избранное'
              : req.notification
        }

        var reqParams = { '_csrf': self.vars.body.data('csrf') }
          , additionalFuncs = additionalFuncs || []
          , addExtendParams = addExtendParams || {}
          , url = ''

        switch(req.type){
          case 'favorites': 
            url = '/user/'+ self.vars.auth_id +'/favorites/'+req.entity+'/'+action+'/'+ $el.data(req.entity+'-id')
            break;

          case 'change':
            url = '/'+req.entity+'/'+ $el.data('id') +'/change'
            reqParams[req.changedField] = (action === 'add')? true : false
            break;
        }

        $.extend(reqParams, addExtendParams);
        
        $.post(url, reqParams)
        .done(function (data) {
          $el[action+'Class'](triggeredClass)
          if (additionalFuncs[funcIndex]) additionalFuncs[funcIndex]()
          self.fn.showNotification('info', notificationText)
        })
        .error(function() {
          self.fn.showNotification('error')
        });


      }


      // global functions

    }

    self._init();

  }



  APP.prototype._init = function() {

    var self = this;

    for (var item in self) {
      if (item.indexOf('_') === -1 && typeof self[item] === 'function') self[item]();
    }

  };



  APP.prototype.higthpriority = function() {

    var self = this;

    $('.a-activeItem').activeItem()
    $('#Tabs').tabulous()
  };


  // ^^ HIGTH PRIORITY FUNCS ^^


  APP.prototype.animateLogo = function() {
    
    var width = 400, height = 520,
        mColor = '#003663',
        ds = {color: mColor, width: 1, opacity: .5},
        df = {color: '#fff', opacity: 0},
        middleX = width/2,
        dot = [
          [80, 300],
          [180, 300],
          [200, 300],
          [200, 230],
          [90, 220],
          [70, 280],
          [25, 160],
          [40, 70],
          [115, 130],
          [100, 35],
          [160, 55],
          [200, 130],
          [135, 185],
          [200,60]
        ],
        shapes = [
          [ [200, 515], [170, 500], [160, 480], [240, 480], [230, 500] ],
          [ [260, 465], [270, 445], [245, 445], [250, 425], [150, 425], [155, 445], [130, 445], [140, 465] ],
          [ [280, 410], [290, 390], [265, 390], [270, 370], [130, 370], [135, 390], [110, 390], [120, 410] ],
          [ [300, 355], [310, 335], [285, 335], [290, 315], [110, 315], [115, 335], [90, 335], [100, 355] ]
        ],
        cfc = 4;


    if (cfc !== 1) {
      middleX /= cfc
      width /= cfc
      height /= cfc

      for (var i = 0; i < shapes.length; i++) {
        shapes[i] = shapes[i].map(function (item) {
          return [item[0]/cfc , item[1]/cfc]
        })
      }

      dot = dot.map(function (item) {
        return [item[0]/cfc , item[1]/cfc]
      })
    }

    var draw = SVG('animated-logo').size(width, height);

    function go($1, $2, $3) {
      var animateSpeed = '200',
          mir1 = [middleX - dot[$1][0] + middleX , dot[$1][1]],
          mir2 = [middleX - dot[$2][0] + middleX , dot[$2][1]],
          mir3 = [middleX - dot[$3][0] + middleX , dot[$3][1]];

      draw.polygon().plot( [mir1, mir2, mir1] ).stroke(ds).fill(df).animate(animateSpeed).plot( [mir1, mir2, mir3] )
      var temp = draw.polygon().plot( [dot[$1], dot[$2], dot[$1]] ).stroke(ds).fill(df).animate(animateSpeed).plot( [dot[$1], dot[$2], dot[$3]] )
      return temp
    }

    for (var i = 0; i < shapes.length; i++) {
      draw.polygon().plot( shapes[i] ).fill(mColor)
    };


    go(0,1,5); go(1,2,3).after(function() {
    go(5,1,4); go(1,3,4).after(function() {
    go(5,4,6).after(function() {
    go(4,6,8).after(function() {
    go(6,8,7).after(function() {
    go(7,8,9).after(function() {
    go(4,8,12); go(4,3,12); go(9,8,10).after(function() {
    go(12,3,11); go(8,10,11).after(function() {
    go(10,11,13)
    }) }) }) }) }) }) }) }) 
    
  };



  APP.prototype.selectize = function() {
    
    var self = this
      , items = $('.a-selectize')
      , temp
      , opts = {

        single: {
          valueField: 'id',
          labelField: 'name',
          searchField: 'name',
          create: false,
          onInitialize: function () {

            var self = this
              , selected = self.$input.data('selected')
              , ajax = self.$input.data('ajax')
              , options = self.$input.data('options')

            function set_selected () {
              if (selected) {
                
                if (isNaN(selected)) {
                  selected.split(',').forEach(function (item) {
                    self.addItem( isNaN(item)? item : item>>0 )
                  })
                } else self.addItem( selected>>0 )
                
              }
            }

            if (options) {
              self.addOption(options)
              set_selected()
            } else if (ajax) {
              $.get(ajax)
              .done(function(data) {
                self.addOption(data)
                set_selected()
              });
            } 
          }
        },

        multi: {
          maxItems: 15
        }
        

      }

    $.extend(opts.multi , opts.single);

    if (!items.length) return;

    for (var i = 0; i < items.length; i++) {
      temp = items.eq(i)
      temp.removeClass('a-selectize').selectize( opts[ temp.data('selectize-type') ] )
    };

  };



  APP.prototype.datepicker = function(first_argument) {
    
    var self = this
      , items = $('.a-datepicker')
      , temp
      , opts = {

        default: {
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd',
          yearRange: '1920:2020'
        }

      }

    if (!items.length) return;
    $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );

    for (var i = 0; i < items.length; i++) {
      temp = items.eq(i)

      temp.removeClass('a-datepicker').datepicker( opts[ temp.data('datepicker-type') || 'default' ] )
    };

  };



  APP.prototype.changeHeader = function() {

    if (!$('#page_event').length) return;
    
    var self = this,
        logo = self.vars.header.find('.logo'),
        nav = self.vars.header.find('.header_nav'),
        blockBottom = self.vars.header.find('.blockBottom'),
        isCollapsed = false,
        isTimeout = true;


    function collapseHeader () {

      TweenMax.to(self.vars.main, .3, {css: { paddingTop: 50 }})

      blockBottom[0].style.zIndex = "-1";
      TweenMax.to(self.vars.header, .3, {css: { top: -140}})
      TweenMax.to(logo, .1, {css:{autoAlpha: 0}})
      TweenMax.to(blockBottom, .2, {css:{top: 0}})
      // TweenMax.to(blockBottom, .2, {css:{y: -180}})
      TweenMax.to(nav, .5, {css: {bottom: 10} })

    }

    function expandHeader () {

      TweenMax.to(self.vars.main, .3, {css: { paddingTop: 360 }})

      TweenMax.to(self.vars.header, .3, {css: { top: 0, marginBottom: 0}})
      TweenMax.to(logo, .8, {css:{autoAlpha: 1}})
      TweenMax.to(blockBottom, .7, {css:{top: 180}, ease:Back.easeOut})
      // TweenMax.to(blockBottom, .7, {css:{y: 0}, ease:Back.easeOut})
      TweenMax.to(nav, .2, {css: {bottom: 200}, onComplete: function () {
        blockBottom[0].style.zIndex = "auto"
      }})

    }

    self.vars.win.on('scroll', function(event) {
      
      if (self.vars.win.scrollTop() > 0 && !isCollapsed && isTimeout) {

        isTimeout = false;

        collapseHeader();
        self.vars.win.scrollTop(1);
        isCollapsed = true;

        setTimeout(function () {
          isTimeout = true;
        }, 600);

      } else if (self.vars.win.scrollTop() === 0 && isCollapsed && isTimeout){
        expandHeader();
        isCollapsed = false;
      }

    });

  };



  APP.prototype.showFull = function() {

    var self = this,
        items = $('.a-showFull'),
        container = $('<div class="showFull-container"></div>'),
        tempText = '',
        lastHoveredElement;

    if (!items.length) return;

    var inEvent = 'mouseenter',
        outEvent = 'mouseleave';

    items.on(inEvent, show);


    function show (el) {

      if (lastHoveredElement) hide.call(lastHoveredElement)

      var el = $(this),
          fontSize = el.css('font-size');

      el.off(inEvent);

      tempText = el.html();
      lastHoveredElement = this;

      container.html(tempText)
      el.html(tempText.slice(0,1))
      el.toggleClass('showFull');
      el.append(container)

      el.on(outEvent, hide);
    }


    function hide () {
      var el = $(this);

      lastHoveredElement = '';
      
      el.off(outEvent);
      el.html(tempText);
      el.toggleClass('showFull');
      container.remove()

      el.on(inEvent, show);
    }

  };


  APP.prototype.ckeditor = function() {
    
    if ( !$('.a-ckeditor')[0] ) return;

    var self = this
      , items = $('.a-ckeditor')
      , opts = {
        maxi: {
          filebrowserImageUploadUrl: '/upload?ckeditor=true&_csrf='+self.vars.csrf,
          extraPlugins: 'image2,divarea', 
          removeButtons: 'Anchor',
          removePlugins: 'stylescombo,specialchar',
          format_tags: 'p;h2;h3;h4;address',
          toolbarGroups: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'styles' },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
            { name: 'links' },
            { name: 'insert' },
            { name: 'tools' },
            // { name: 'document',    groups: [ 'mode' ] }
          ]
        }, 
        mini: {
          filebrowserImageUploadUrl: '/upload?ckeditor=true',
          extraPlugins: 'image2,divarea',
          removeButtons: 'Anchor',
          removePlugins: 'stylescombo,specialchar',
          format_tags: 'p;h2;h3;h4;address',
          toolbarGroups: [
            { name: 'insert' },
            { name: 'tools' },

          ]
        },
        event: {
          filebrowserImageUploadUrl: '/upload?ckeditor=true',
          extraPlugins: 'image2,divarea', 
          removeButtons: 'Anchor',
          removePlugins: 'stylescombo,specialchar',
          format_tags: 'p;h4',
          toolbarGroups: [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'styles' },
            { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
            { name: 'links' },
            { name: 'insert' },
            { name: 'tools' },
          ]
        },
      }

    items.each(function(index, el) {
      var $el = $(el)
        , id = 'ckeditor'+self.vars.ckeditor_counter
      $el.attr('id', id).removeClass('a-ckeditor')
      CKEDITOR.replace(id, opts[ $el.data('ck-type') ] )
      self.vars.ckeditor_counter ++
    });

    // CKEDITOR.on('dialogDefinition', function (ev) {
    //   console.log(ev);
    //   var dialogName = ev.data.name;
    //   var dialogDefinition = ev.data.definition;
    //   if (dialogName == 'image2') {
    //     console.log(dialogDefinition);
    //     dialogDefinition.contents[1].elements[0].action += '&_csrf='+self.vars.csrf;
    //   }
    // });

  };


  APP.prototype.knowledgeTabs = function() {

    if ( !$('.Knowledge')[0] ) return;

    var self = this
      , items = $('.Knowledge').find('.blockTop')

    items.on('click', expand);


    function expand (event) {
      event.stopPropagation()
      $(this).css('border-bottom', '1px solid #21447a').siblings('.blockBottom').slideDown().end().off('click').on('click', collapse);
    }


    function collapse (event) {
      event.stopPropagation()
      var $this = $(this)
      $this.siblings('.blockBottom').slideUp(function () {
        if (items.length === 1) return;
        $this.css('border-bottom', 'none')
      }).end().off('click').on('click', expand);
    }

  };



  // ---- LOW PRIORITY FUNCS ----

    APP.prototype.searchGlobalHandler = function() {
    
    var self = this,
      el = $('#search-global'),
      button = el.find('.button'),
      input = el.find('.search-global_input'),
      isClosed = true;


    el.on('click', function(event) {
      event.preventDefault();
      event.stopPropagation();
    });


    input.on('focus', function(event) {
      self.vars.doc.on('keypress.searchEnable', function(event) {
        if (event.which == 13) button.trigger('click')
      });
    });


    input.on('blur', function(event) {
      self.vars.doc.off('keypress.searchEnable');
    });


    button.on('click', function(event) {
      
      if (isClosed) {

        TweenMax.to(input, .3, {css: {width: "150px"}})
        input.focus();
        isClosed = false;

        self.vars.body.on('click.searchClose', function(event) {
          
          TweenMax.to(input, .3, {css: {width: "0px"}})
          isClosed = true;
          self.vars.body.off('click.searchClose')

        });

      } else {
        window.location.href = '/search?entity=post&text=' + input.val()
      }
      
    });

  };


  // Оверлэй и вспывающие окна

  APP.prototype.editEndOfWord = function() {

    var self = this
    
    $('.a-eeow').each(function(index, el) {
      var $el = $(el)
        , opt = $el.data('eeow').split(',')

      $el.html( wordPostfix(opt[0], opt[1], opt[2], opt[3]) )
    });

    function wordPostfix(num, word, word1, word2) {
 
      var number = (typeof num == 'number')? String(num) : num,
          result = word;
     
      if (number.slice(-2, -1) != "1") {
        if (number.slice(-1) == "2" ||
            number.slice(-1) == "3" ||
            number.slice(-1) == "4") result = word2;
        else if (number.slice(-1) == "1") result = word1;
      }
    
      return number +' '+ result;
    }

  };



  APP.prototype.overlay = function() {
    
    var self = this
      , blocks = {
          signin: $('.b-popup-signin'),
          signup: $('.b-popup-signup'),
          newMessage: $('.b-popup-newMessage'),
          upload: $('.b-popup-upload'),
          uploadInGallery: $('.b-popup-uploadInGallery'),
          enroll: $('.b-popup-eventEnroll')
        }
      , buttons = {
          openSignin: $('.a-popup-signin'),
          openSignup: $('.a-popup-signup'),
          openNewMessage: $('.a-popup-newMessage'),
          openUploadInGallery: $('.a-popup-uploadInGallery'),
          openEnroll: $('.a-popup-eventEnroll')
        }
      , popup = self.vars.popup
      , upload_target_field
      , lastY = 0
      , edit_page = $('#page_edit')
      , headerHeigth = self.vars.header.innerHeight()
      , accuracy = 50
      , acc_hH = headerHeigth + accuracy


    function openPopup (data) {
      self.vars.popup.empty().append(data);
      self.vars.overlay.css('height', self.vars.doc.innerHeight())
      self.vars.overlay.show();
      self.vars.win.trigger('scroll')
      self.vars.popup.css('margin-left', -self.vars.popup.innerWidth()/2);
      TweenMax.from(self.vars.popup, .5, {css: {autoAlpha: 0, y: -100}, ease:Back.easeOut})
      setTimeout(function(){self.vars.win.trigger('scroll')}, 10);
      
    }

    function closePopup () {
      self.vars.overlay.fadeOut('200');
    }

    self.vars.overlay.on('click', function(event) {
      event.stopPropagation();
      closePopup()
    });

    self.vars.popup.on('click', function(event) {
      event.stopPropagation();
    });


    buttons.openNewMessage.on('click', function(event) {
      blocks.newMessage.find('input[name="recipient_id"]').val( $(this).data('user-id') )
      blocks.newMessage.find('textarea').val('')
      openPopup( blocks.newMessage )
    });

    
    if (edit_page.length) {
      edit_page.on('click', '.a-popup-upload', function(event) {
        var $this = $(this)
          , target = $this.data('target')
        upload_target_field = target? self.vars.body.find('input[name="'+target+'"]') : ''
        blocks.upload.find('input[name=type]').val( $this.data('upload-type') )
        openPopup( blocks.upload )
      });

      buttons.openUploadInGallery.on('click', function(event) {
        var $this = $(this)
          , gallery = $this.data('gallery')

      });

      popup.on('submit', '#newMessage', function(event) {
        event.preventDefault();
        $.post('/upload?'+ $(this).serialize() )
        .done(function () {
          closePopup()
          self.fn.showNotification('info', 'Сообщение отправлено')
        })
        .error(function() {
          self.fn.showNotification('error')
        });
      });

    };

    buttons.openSignin.on('click', function(event) {
      openPopup( blocks.signin )
    });

    buttons.openSignup.on('click', function(event) {
      openPopup( blocks.signup )
    });

    buttons.openEnroll.on('click', function(event) {
      openPopup( blocks.enroll )
    });

    popup.on('submit', '#newMessage', function(event) {
      event.preventDefault();
      $.post('/message?'+ $(this).serialize() )
      .done(function () {
        closePopup()
        self.fn.showNotification('info', 'Сообщение отправлено')
      })
      .error(function() {
        self.fn.showNotification('error')
      });
    });


    popup.on('submit', '#upload', function(event) {
      event.preventDefault();

      $.ajax({
        url: '/upload',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false
      })
      .done(function (data) {
        closePopup()
        if (upload_target_field) {
          upload_target_field.val(data).siblings('img').attr('src', data);
        }
        console.log(data);
        self.fn.showNotification('info', 'Фаил успешно загружен')
      })
      .error(function() {
        self.fn.showNotification('error')
      });
    });


    self.vars.win.on('resize', function(event) {
      self.vars.overlay.css('height', self.vars.doc.innerHeight())
    })

    self.vars.win.on('scroll', function(event) {
      
      var Y = self.vars.win.scrollTop(),
          winHeigth = self.vars.win.innerHeight(),
          popupHeigth = popup.innerHeight();

      if (winHeigth > popupHeigth + acc_hH) {
        if (popup.css('position') != 'fixed') popup.css({'position':'fixed', 'top': acc_hH});
        return;
      } 

      popup.css('position', 'absolute');

      if (Y > lastY) {

        if (Y + winHeigth > popupHeigth + popup.offset().top + accuracy) {
          popup.css('top', Y + winHeigth - popupHeigth - accuracy)
        }

      } else {

        if (Y < popup.offset().top - acc_hH) {
          popup.css('top', Y + accuracy)
        }

      }

      lastY = Y;

    });

  };



  APP.prototype.authentificated = function() {
    
    var self = this
      , root = $('#Authentificated')
      , buttonMenu = root.find('.button')
      , menu = root.find('.auth_menu')

    if ( !root[0] ) return;

    buttonMenu.on('click', openMenu )
    

    function openMenu (event) {
      event.stopPropagation()
      menu.show()
      buttonMenu.off('click'); 
      buttonMenu.on('click', closeMenu )
      self.vars.body.on('click.auth_menu', closeMenu);
    }

    function closeMenu (event) {
      menu.hide()
      buttonMenu.off('click');
      self.vars.body.off('click.auth_menu');
      buttonMenu.on('click', openMenu )
    }

  };



  APP.prototype.notification = function() {

    var self = this
    
    $('#Notification').on('click', '.item', function(event) {
      $(this).off('click').fadeOut(function () {
        $(this).remove()
      });
    });

  };


  APP.prototype.comments = function() {
    
    if ( !$('#Authentificated')[0]  ) return;
    if ( !$('.Comment')[0]  ) return;

    var self = this
      , root = $('.Comment')
      , form = $('#createMessage')
      , parent_id = form.find('[name=parent_id]')
      , textarea = form.find('[name=html]')
      , submit = form.find('.button')
      , triggerForm = false
      , items = root.find('.item')
      , buttonAnswer = $('<span class="button press toRight a-commentAnswer">ответить</span>')
      , buttonEdit= $('<span class="button press toRight a-commentEdit">редактировать</span>')
    

    function entered () {
      var $this = $(this)
        , auth_id = self.vars.auth.data_id >> 0
        , author_id = $this.data('author-id') >> 0

      $this.prepend(buttonAnswer)
      if (auth_id === author_id) $this.prepend(buttonEdit);
    }

    function leaved () {
      buttonAnswer.remove()
      buttonEdit.remove()
    }

    function clearForm () {
      form.attr('action', '/comment');
      submit.html( 'отправить' )
      textarea.val('')
      triggerForm = false
    }


    items.on('mouseenter', entered);
    items.on('mouseleave', leaved);

    root.on('click', '.a-commentAnswer',function(event) {
      if (triggerForm) clearForm();
      var comment = $(this).closest('.item')
      parent_id.val( comment.data('id') ) 
      comment.append(form)
    });

    root.on('click', '.a-commentEdit', function(event) {
      var comment = $(this).closest('.item')
        , html = comment.find('.html').html()

      triggerForm = true
      form.attr('action', '/comment/'+comment.data('id'));
      submit.html( 'отредактировать' )
      textarea.val( html )

      comment.append(form)
      
    });

  };


  APP.prototype.tags = function() {
    
    if ( !$('#Authentificated')[0]  ) return;
    if ( !$('.Tag')[0]  ) return;

    var self = this
      , buttonSubscribe = $('.Tag').find('.a-subscribeToTag')

    buttonSubscribe.on('click', function(event) {
      
      var $this = $(this)

      self.fn.requestHandler($this ,'in_interests', {type: 'favorites' ,entity: 'tag', entityName: 'Тэг'})

    });

  };


  APP.prototype.events = function() {
    
    if ( !$('#Authentificated')[0]  ) return;
    if ( !$('.Event')[0]  ) return;

    var self = this
      , buttonSubscribe = $('.Event').find('.a-subscribeToEvent')

    buttonSubscribe.on('click', function(event) {
      var $this = $(this)
      self.fn.requestHandler($this ,'subscribed', {type: 'favorites' ,entity: 'event', entityName: 'Событие'})
    });

  };


  APP.prototype.posts = function() {
    
    if ( !$('#Authentificated')[0]  ) return;
    if ( !$('.Post')[0]  ) return;

    var self = this
      , buttonSubscribe = $('.Post').find('.a-subscribeToPost')

    buttonSubscribe.on('click', function(event) {
      
      var $this = $(this)
        , countComments = $this.siblings('.countComments').find('.counterComments').text() >> 0
        , fieldSubscribers = $this.find('.counterSubscribers')
        , countSubscribers = fieldSubscribers.text() >> 0
        , additionalFuncs = [
            function () {
              fieldSubscribers.text( countSubscribers-1 )
            },

            function () {
              fieldSubscribers.text( countSubscribers+1 )
            },
          ]
        , addExtendParams = {'countComments': countComments}


      self.fn.requestHandler($this ,'subscribed', {type: 'favorites', entity:'post', entityName:'Пост'}, addExtendParams, additionalFuncs)

    });

  };


  APP.prototype.users = function() {

    if ( !$('#Authentificated')[0]  ) return;
    if ( !$('.User')[0]  ) return;

    var self = this

    $('.a-userToFavorites').on('click', function(event) {

      var $this = $(this)

      self.fn.requestHandler($this ,'active', {type: 'favorites', entity: 'user', entityName: 'Пользователь'})
      
    });

  };


  APP.prototype.companys = function() {
    
    if ( !$('#Authentificated')[0]  ) return;
    if ( !$('#page_company_item')[0]  ) return;

    var self = this

    $('.a-userToWriters').on('click', function(event) {
      
        var $this = $(this)
        , additionalFuncs = [
            function () {
              $this.text('Назначить писателем')
            },

            function () {
              $this.text('Убрать из писателей')
            },
          ]


      self.fn.requestHandler($this ,'active', {
        type: 'change',
        entity: 'user',
        changedField: 'is_writer',
        notification: 'Статус пользователя изменен', 
      }, {}, additionalFuncs)
      
      
    });


    $('.a-userToSpeakers').on('click', function(event) {
      
        var $this = $(this)
        , additionalFuncs = [
            function () {
              $this.text('Назначить лектором')
            },

            function () {
              $this.text('Убрать из лекторов')
            },
          ]


      self.fn.requestHandler($this ,'active', {
        type: 'change',
        entity: 'user',
        changedField: 'is_speaker',
        notification: 'Статус пользователя изменен', 
      }, {}, additionalFuncs)
      
      
    });

  };



  APP.prototype.edit = function() {

    if (!$('#page_edit')[0]) return;

    var self = this
      , page = $('#page_edit')
      , hideTimer
      , patterns = page.find('.a-pattern')
      , buttonDelete = $('<div style="z-index: 100; display: none; right: -16px" class="a-deleteAdditionalFiel" title="Удалить поле">&#8211;</div>')
    
    patterns.find('[required]').each(function(index, el) {
      var $el = $(el)
      $el.attr('required', null)
      $el.attr('data-required', 'true')
    });

    function show_delete_button (event) {
      event.stopPropagation();
      var $this = $(this)
      clearTimeout(hideTimer)
      buttonDelete.css('top', $this.position().top+17+'px' ).off('click').on('click', function(event) {
        $this.remove()
        buttonDelete.hide()
      });

      if (this.tagName.toLowerCase() === 'input') $this.closest('.col2').append(buttonDelete)
      else $this.append(buttonDelete)

      $this.on('mouseleave', hide_delete_button)
      buttonDelete.show()
    }


    function hide_delete_button (event) {
      event.stopPropagation();
      $(this).off('mouseleave')
      hideTimer = setTimeout(function(){buttonDelete.hide()}, 2000);
    }


    page.on('change', '.a-pseudoField', function(event) {
      var connector = $(this).data('connector')
        , aggregatedData = $('.a-pseudoField').filter('[data-connector='+connector+']').map(function (i, item) {return $(item).val()}).toArray().join('=')
      console.log(connector, aggregatedData);
      $('#'+connector).val(aggregatedData)
    });

    page.on('mouseenter', '.a-canBeDeleted', show_delete_button);

    page.on('click', '.a-createAdditionalField', function(event) {
      
      var $this = $(this)
        , type = $this.data('field-type')

      switch(type){
        case 'simple':
          var original = $this.children('.a-pattern')
            , parent = $this.closest('.fieldSet')
            , clone = original.clone().val('').toggleClass('a-pattern a-canBeDeleted').attr('name', original.data('name'));

          parent.append(clone)
          break;


        case 'pseudo':
          var original = $this.children('.a-pattern')
            , now_sets = $this.siblings('.itemSet')
            , connector = now_sets.length? now_sets.last().children()[0].id.replace(/\d+$/, function (num) {return (num>>0)+1}) : original.children()[0].id
            , clone = original.clone().toggleClass('a-pattern a-canBeDeleted')

          clone.find('[data-name]').each(function(index, el) {
            var $el = $(el)
            $el.attr('name', $el.data('name'));
          });

          clone.find('.a-pseudoField').each(function (i, item) {
            var $item = $(item)
            if (item.tagName.toLowerCase() === 'select') $item.addClass('a-selectize').find('option').remove()
            $item.attr('data-connector', connector).val('')
          })

          clone.find('input[type=hidden]').attr({
            'id': connector,
            'value': '',
            'name': connector.replace(/(:?for_)(\D+)(_\d+)$/, function (str, s0, s1, s2) {return s1+'['+s2.replace('_','')+']'})
          })

          $this.parent().append(clone)
          self.datepicker()
          self.selectize()
          break;


        case 'itemset':
          var original = $this.children('.a-pattern')
            , setname = original.data('setname')
            , regexp = new RegExp(setname+'\\D*(\\d+)')
            , now_sets = $this.siblings('.itemSet')
            , counter = now_sets.length? (now_sets.last().find('[name]').attr('name').match(regexp)[1] >> 0)+1 : 0
            , clone = original.clone().toggleClass('a-pattern a-canBeDeleted')
            , nested_pattern = clone.find('.a-pattern')
            , temp_place
            , temp

          if (nested_pattern.length) {
            temp = nested_pattern.first()
            temp_place = temp.parent()
            temp.remove()
          }

          function change_attr ($el, attr_name, source) {
            $el.attr(attr_name, source.replace(regexp, function (str, s1) {
              return str.slice(0, -(s1.length) ) + counter
            }) ).val('')
          }

          clone.find('[data-name]').each(function (i, item) {
            var $item = $(item)
              , name =  $item.data('name')
            if (name==='id') return $item.remove()
            if (item.tagName.toLowerCase() === 'select') $item.addClass('a-selectize').find('option').remove()
            else if (item.tagName.toLowerCase() === 'textarea' && $item.data('ck-type')) $item.addClass('a-ckeditor')
            else if ($item.hasClass('hasDatepicker')) $item.toggleClass('hasDatepicker a-datepicker').removeAttr('id')
            if ( $item.data('required') ) $item.attr('required', 'true')
            change_attr($item, 'name', name)
          })

          clone.find('.a-popup-upload').each(function (i, item) {
            var $item = $(item)
            $item.attr('data-target', $item.prev('input').attr('name'))
          })

          if (temp) {
            temp.find('[data-name]').each(function (i, item) {
              var $item = $(item)
                , name =  $item.data('name')
              if (name==='id') return $item.remove()
              change_attr($item, 'data-name', name)
            })
            temp_place.append(temp)
          };

          $this.parent().append(clone)
          self.selectize()
          self.datepicker()
          self.ckeditor()
          break;

      }
    });

  };


  APP.prototype.enrolledToEvent = function() {
    
    if ( !$('#Authentificated')[0]  ) return;
    if ( !$('#page_event_enrolled')[0]  ) return;

    var self = this

        $('.a-userIsWent').on('click', function(event) {
      
        var $this = $(this)
        , additionalFuncs = [
            function () {
              $this.text('Ещё не ходил')
            },

            function () {
              $this.text('Уже сходил')
            },
          ]


      self.fn.requestHandler($this ,'active', {
        type: 'change',
        entity: 'event',
        changedField: 'is_went',
        notification: 'Статус пользователя изменен', 
      }, {}, additionalFuncs)
      
      
    });


    $('.a-userIsHandled').on('click', function(event) {
      
        var $this = $(this)
        , additionalFuncs = [
            function () {
              $this.text('Запрос не обработан')
            },

            function () {
              $this.text('Запрос обработан')
            },
          ]


      self.fn.requestHandler($this ,'active', {
        type: 'change',
        entity: 'event',
        changedField: 'is_handled',
        notification: 'Статус пользователя изменен', 
      }, {}, additionalFuncs)
      
      
    });

  };


  APP.prototype.isles = function() {
    
    var self = this
      , isles = $('.isle').filter(function(index, item) {
        var h = $(item).find('.isle-helper')
        if (!h.length) return false;
        h.css('min-width', h.children().map(function(index, elem) {return $(elem).innerWidth()})
          .toArray().reduce(function (a,b) {return a+b}) )
        return true
      })

    if (!isles.length) return

    isles.hover(function() {
      var helper = $(this).find('.isle-helper')
      helper.css('margin-left', -helper.innerWidth());
    }, function() {
      var helper = $(this).find('.isle-helper')
      helper.css('margin-left', 0);
    });

  };



  APP.prototype.map = function() {
    
    var self = this
      , map
      , mapEl = $("#googleMap")

    if (!mapEl[0]) return

    var coords = mapEl.data('coords').split(',').map(function (item) {return parseFloat(item)})
    function initialize() {
      var LatLng = new google.maps.LatLng(coords[0], coords[1])
        , style = [
            {
              "stylers": [
                { "hue": "#003bff" },
                { "saturation": 90 },
                { "gamma": 0.5 },
                { "weight": 1 }
              ]
            }
          ]
        , mapOptions = {
            zoom: 15,
            center: LatLng
          }
        , styledMap = new google.maps.StyledMapType(style,{name: "Styled Map"})
        , map = new google.maps.Map(document.getElementById('googleMap'),
            mapOptions)
        , marker = new google.maps.Marker({
            position: LatLng,
            map: map,
            icon: '/images/icons/map_marker.png'
            //- title: 'Hello World!'
          });

      map.mapTypes.set('map_style', styledMap);
      map.setMapTypeId('map_style');
    }

    google.maps.event.addDomListener(window, 'load', initialize);

  };


  APP.prototype.lowpriority = function() {

    var self = this

    self.vars.overlay.css('height', self.vars.doc.innerHeight())
  };





  // Инициализация приложения
  app = new APP;

})(jQuery)


